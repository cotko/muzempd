'use strict'

global._ = require('lodash')
global.P = require('bluebird')
global.gulp = require('gulp')
global.gulpPlugs = require('gulp-load-plugins')()

const requireDir = require('require-dir')

exports = module.exports = (conf) => {

  let watch = _
    .chain(process.argv)
    .map( v => v.toLowerCase() )
    .indexOf('--watch')
    .thru( idx => !!~idx )
    .value()

  console.log('runngin gulp, watching:', watch)

  let src = _.get(conf, 'src', './src')
  let dest = _.get(conf, 'dest', './dest')

  global.onError = tag => {
    return err => {
      console.log(err)
      gulpPlugs.notify.onError(`${tag} err\n<%= error.message %>`)(err)
    }
  }

  global.config = _.merge({
    watch,
    src,
    dest,
    html: {
      src: `${src}/ui/pug/**/*.pug`,
      dest: `${dest}/html`
    },
    styles: {
      src: `${src}/ui/styles/index.less`,
      dest: `${dest}/css`,
      src_watch: [`${src}/ui/styles/**/*.less`]
    },
    babel: {
      src: [
        `${src}/**/*.jsx`,
        `${src}/**/*.js`
      ],
      dest: `${dest}`,
      conf: {
        presets: ['react'],
        plugins: [
          ['transform-es2015-modules-commonjs', {
            'allowTopLevelThis': false,
            'strict': false,
            'loose': false
          }]
        ]
      }
    },
    copy: [
    {
      src: `${src}/ui/styles/vendor/**/*.css`,
      dest: `${dest}/css/vendor`
    },
    {
      src: `${src}/ui/fonts/**/*`,
      dest: `${dest}/fonts`
    }]
  }, conf)

  // require all tasks
  requireDir('./tasks', { recurse: true })

}
