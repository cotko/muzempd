'use stict'

let conf = config.copy

gulp.task(
  'copy',
  () => {
    let promises = []
    for(let cpy of conf) {
      promises.push(
        gulp
          .src(cpy.src)
          .pipe(gulp.dest(cpy.dest))
      )
    }
    return P.all(promises)
  }
);
