'use strict'


global.P = require('bluebird')
global._ = require('lodash')
global.moment = require('moment')

const {
  Logger,
  Lng,
  Err,
  Util
} = require('../lib')

const Log = new Logger('BOOTSTRAP')

global.ERR = window.ERR = Err
global.Logger = window.Logger = Logger
global.UTIL = window.UTIL = Util

const { UI } = require('../ui/js')
const bridge = require('../bridge')
const path = require('path')

global.PROVIDERS = require('../lib/providers')

const LOCALE = Lng({
  directory: path.join(__dirname, 'i18n'),
  register: global
})
window.__ = global.__
global.LOCALE = window.LOCALE = LOCALE


global.bridge = require('../bridge')



const init = () => {

  global.NW = {
    nw,
    win: nw.Window.get()
  }

  NW.win.showDevTools()

  return startUI()
}

const startUI = () => {
  return P
    .resolve()
    .then( () => {
      UI.render(
        'wrapper',
        bridge.statestream.$
      )
    })
    .then( handleUI )
    .delay( 500 )
    .then( startApp )
}

const handleUI = () => {
  return Promise
    .resolve()
    .then( () => {
      // subscribe to window actions
      bridge
        .uisubject
        .$
        .filter( d => 'winaction' === _.get(d, 'identifier') )
        .map( d => _.get(d, 'ctx') )
        .subscribe( action => {
          Log.l('ui action', action)

          switch(action) {
            case 'exit':
              cleanup()
              .then( () => {
                NW.win.close()
              })
              break;
            case 'maximize':
              break;
            case 'minimize':
              break;
            case 'reload':
              cleanup()
              .then( () => {
                NW.win.reloadIgnoringCache()
              })
              break;
          }
        })

    })
}

const startApp = () => {
  Log.l('startApp()')
  return bridge
    .bridge
    .start()
}

const cleanup = () => {
  Log.l('cleanup()')
  return P
    .resolve(UI.shutdown())
    .then( () => bridge.shutdown() )

}

global.CLEANUP = cleanup

init()
