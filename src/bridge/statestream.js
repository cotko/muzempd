'use strict'

const Log = Logger.get(__filename)

// App state stream
//
// collects other streams and combines them
// into one.

const Rx = require('rx')

const {
  bridge,
  services
} = require('./')

const { Err } = require('../lib')

let stream$ = null

const shutdown = () => {
  Log.i('shutdown()')

  return P
    .resolve()
    .then( () => Err.dispose() )
}


module.exports = {

  shutdown,

  get $() {

    stream$ = stream$ || Rx
      .Observable
      .combineLatest(
        services.$,
        Err.$.subject,
        (services, errors) => ({
          services,
          errors,
          // add UI hanlder
          request: bridge.request,
          $: {
            search: bridge.search,
            search$: bridge.search$
          }
        })
      )

    return stream$
  }
}
