'use strict'

const Log = Logger.get(__filename)

// Simple UI subject for UI events
//

const Rx = require('rx')


let subject$ = null

const shutdown = () => {
  Log.i('shutdown()', !!subject$)
  if(subject$)
    subject$.dispose()
}


module.exports = {

  shutdown,

  get $() {
    subject$ = subject$ || new Rx.Subject()
    return subject$
  }
}
