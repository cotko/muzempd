'use strict'

const Log = Logger.get(__filename)

// Hanldes all the services
//


const { MPD } = require('../../lib')
const { uisubject } = require('../')
const Rx = require('rx')

const services = require('./services')

global.MPD = MPD.MPD


// will hold stream of services
let services$ = null
let uisubscription = null

const start = () => {
  Log.i('start()')
  return P
  .resolve()
  .then( () => {
    uisubscription = uisubject
      .$
      .filter( req => {
        let id = _.get(req, 'identifier', '')
        if(_.isString(id) && id.startsWith('service.'))
          return true
      })
      .map( req => {
        let [foo, service, action] = req.identifier.split('.')
        return {
          service,
          action,
          ctx: req.ctx
        }
      })
      .subscribe(
        req => {
          Log.d('start() handle request', req)
          try {
            services[req.service][req.action](req.ctx)
          } catch (err) {
            console.log('ererer', err)
          }
        }
      )
  })
  .then( () => MPD.MPD.retryUntillConnected() )
}

const getStream = () => {
  services$ = services$ || Rx
    .Observable
    .combineLatest(
      MPD.MPD.$.Stream.$,
      (mpd) => ({
        mpd
      })
    )
    .map( addStateProps )

  return services$
}

const addStateProps = state => {
  // check if mpd is found or not
  let mpdconnected = _.get(state, 'mpd.connected', false)
  if(!mpdconnected)
    state.ready = false
  else
    state.ready = true
  // check if we need to display the
  // config form
  let retries = _.get(state, 'mpd.probe.autoconnect.retry_count')
  if(retries > 2)
    state.show_mpd_config = true
  return state
}

const shutdown = () => {
  Log.i('shutdown()')
  return P
  .resolve()
  .then( () => {
    if(uisubscription)
      uisubscription.dispose()
    uisubscription = null
  })
  .then( () => {
    return MPD
    .MPD
    .dispose()
  })
  .catch( e => Log.e(e, e.stack) )
}

module.exports = {
  get $() { return getStream() },
  start,
  shutdown
}
