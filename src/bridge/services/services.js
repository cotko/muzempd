'use strict'

module.exports = {
  get mpd() { return require('./mpd') },
  get shcast() { return require('./shcast') }
}
