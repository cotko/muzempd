'use strict'

const {MPD: { MPD: mpd }} = require('../../lib')



exports.connect  = mpd.connect.bind(mpd)

// player
exports.pause    = mpd.api.Player.pause.bind(mpd)
exports.playid   = mpd.api.Player.playid.bind(mpd)
exports.playpos  = mpd.api.Player.playpos.bind(mpd)
exports.stop     = mpd.api.Player.stop.bind(mpd)
exports.previous = mpd.api.Player.previous.bind(mpd)
exports.next     = mpd.api.Player.next.bind(mpd)
exports.seek     = mpd.api.Player.seek.bind(mpd)
exports.setVol   = mpd.api.Player.setVol.bind(mpd)
exports.search   = mpd.api.Database.search.bind(mpd)
