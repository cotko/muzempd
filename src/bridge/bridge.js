'use strict'

const Log = Logger.get(__filename)

// Handler for UI actions
// adds methods for triggering UI requests
// and handles a part of them
//

const {
  uisubject,
  services,
  search
} = require('./')

const request = (identifier, ctx) => {

  Log.l('request()', identifier, ctx)

  return uisubject.$.onNext({
    identifier,
    ctx
  })
}

const start = () => {
  Log.i('start()')
  return services
    .start()
}

const shutdown = () => {
  Log.i('shutdown()')
  return P
    .resolve()
    .then( () => search.shutdown() )
    .then( () => services.shutdown() )
}


module.exports = {
  start,
  shutdown,
  request,
  search: search.search,
  search$: search.search$
}
