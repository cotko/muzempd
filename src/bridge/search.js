'use strict'

// Searchable stream


const Log = Logger.get(__filename)
const Rx = require('rx')
const services = require('./services/services')

const search$ = new Rx.BehaviorSubject({
  filter: '',
  result: {},
  state: {}
})

const filter$ = new Rx.Subject()

const subscription = filter$
  .map( x => {
    console.log('jebi si mater', x, typeof x, 'string' === typeof x)
    if('string' === typeof x) {
      x = x.trim()
      if(!x) return null
      console.log('kle jebemtimater returnam')
      return {
        tags: null,
        filter: x
      }
    }
    return x
  })
  .filter( x => !!x )
  .subscribe( ctx => {
    Log.i('filter$subscription', ctx)
    search(ctx)
  })

const search = ctx => {
  let { filter } = ctx
  let results = {
    filter,
    result: {},
    state: {}
  }

  search$.onNext( results )

  for(let svc in services) {
    let service = services[svc]

    Log.d(`#search():${svc}...`, ctx)

    results.state[svc] = 'searching'
    search$.onNext( results )

    service
      .search(ctx.filter, ctx.tags)
      .then( res => {
        results.state[svc] = 'done'
        results.result[svc] = res
      })
      .catch( err => {
        Log.e(`#search():${svc}`, err)
        results.state[svc] = 'error'
        results.result[svc] = err
      })
      .finally( () => {
        search$.onNext( results )
      })
  }
}

const shutdown = () => {
  Log.i('#shutdown()')
  subscription.dispose()
  filter$.onCompleted()
  search$.onCompleted()
}

module.exports = {
  get search$() { return search$ },
  search: ctx => { filter$.onNext(ctx) },
  shutdown
}
