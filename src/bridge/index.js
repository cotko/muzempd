'use strict'

const Log = Logger.get(__filename)

const shutdown = () => {
  Log.i('shutdown()')
  return P
    .resolve()
    .then( bridge.statestream.shutdown )
    .then( bridge.uisubject.shutdown )
    .then( bridge.bridge.shutdown )

}

const bridge = {
  shutdown,
  get statestream() { return require('./statestream') },
  get services() { return require('./services') },
  // used for UI event stream
  get uisubject() { return require('./uisubject') },
  get bridge() { return require('./bridge') },
  get search() { return require('./search') }
}

module.exports = bridge
