'use strict'

export class MPDConfField extends React.Component {

  constructor(props) {
    super(props)

    this.state = this.getState(props)
  }

  getState(props=this.props) {
    return {
      conf_str: _.get(props, 'value', '')
    }
  }

  componentWillReceiveProps(next_props) {
    this.setState(this.getState(next_props))
  }

  _onKeyDown(evt) {
    switch(evt.key) {
      case 'Enter':
        this.props.submit()
        break
      case 'Escape':
        this.props.clear()
        break
    }
  }

  render() {
    let atts = []
    return (
      <input
        type='url'
        readOnly={ !this.props.enabled }
        disabled={ !this.props.enabled }
        value={ this.state.conf_str }
        onChange={ evt => this.props.onChange(evt.target.value) }
        onKeyDown={ this._onKeyDown.bind(this) }
        placeholder={ __('MPD url:port or path/to/socket') }
        />
    )

  }

}
