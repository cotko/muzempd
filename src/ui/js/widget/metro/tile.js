'use strict'

const renderTile = (props, tclasses={}) => {

  let tileprops = _.omit(props, [
    'bg',
    'fg',
    'icon',
    'label',
    'className'
  ])

  let cn = CNP(props)
  if(props.bg) tclasses[`bg-${props.bg}`] = true
  if(props.fg) tclasses[`fg-${props.fg}`] = true

  let label = props.label
  if('string' === typeof label)
    label =
      <span className='tile-label'>
        { label }
      </span>
  else
    label = false

  let content = false
  if('string' === typeof props.icon)
    content =
      <span className='tile-content iconic'>
        <span className={`icon mif-${props.icon}`} />
        { props.children }
      </span>
  else
    content =
      <span className='tile-content'>
        { props.children }
      </span>

  return (
    <div { ...tileprops } className={ cn(tclasses) }>
      { content }
      { label }
    </div>
  )
}

export class Tile extends React.Component {
  render() {
    return renderTile(
      this.props, {
        'tile': true
      }
    )
  }
}

export class TileSmall extends React.Component {
  render() {
    return renderTile(
      this.props, {
        'tile-small': true
      }
    )
  }
}

export class TileWide extends React.Component {
  render() {
    return renderTile(
      this.props, {
        'tile-wide': true
      }
    )
  }
}

export class TileLarge extends React.Component {
  render() {
    return renderTile(
      this.props, {
        'tile-large': true
      }
    )
  }
}
