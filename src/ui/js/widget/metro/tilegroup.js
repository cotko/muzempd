'use strict'

export class TileGroup extends React.Component {

  constructor(props) {
    super(props)

    this.state = this.getState(props)
  }

  componentWillReceiveProps(next_props) {
    this.setState( this.getState(next_props) )
  }

  getState(props=this.props) {
    // extract props for the container
    let valid_props = _.omit(props, [
      'size',
      'label'
    ])

    let {
      size,
      label
    } = props

    // add classes
    let className = { 'tile-group': true }
    if(size) className[size] = true
    valid_props.className = CNP(props)(className)

    return {
      props: valid_props,
      label
    }
  }

  renderLabel() {
    if(!this.state.label) return
    return(
      <span className='tile-group-title'>
        { this.state.label }
      </span>
    )
  }

  render() {
    return(
      <div { ...this.state.props }>
        { this.renderLabel() }
        <div className='tile-container'>
          { this.props.children }
        </div>
      </div>
    )
  }

}

TileGroup.SIZE = {
  NONE:  '',
  ONE:   'one',
  TWO:   'two',
  THREE: 'three',
  FOUR:  'four',
  FIVE:  'five',
  SIX:   'six',
  SEVEN: 'seven'
}
