'use strict'

const metroWidgets = {}

for(let widget of [
  'tile',
  'slider',
  'tilegroup'
]) {

  let mwidget = require(`./${widget}`)
  _.merge(metroWidgets, mwidget)

}

module.exports = metroWidgets
