'use strict'

export class Slider extends React.Component {

  constructor(props) {
    super(props)
    this.state = _.merge({
      styles: {}
    },
      this.getValues(props),
      this.getState(props)
    )

    this.onVolThrottled = _.throttle(this.onVol, 80)
    this.onWheelThrottled = _.throttle(this.onWheel, 10)
    this.updateFromPropsDebounced = _.debounce(this.updateFromProps, 500)

    this.unsetCooldown = _.debounce( () => {
      delete this._cooldown
    }, 300)

    this._size = {
      slider: [0, 0],
      marker: [0 ,0]
    }
  }

  _getSize() {
    if(!this._mounted) return 0
    let snode = ReactDOM.findDOMNode(this.refs.slider)
    let mnode = ReactDOM.findDOMNode(this.refs.marker)
    return {
      slider: [
        snode.scrollWidth,
        snode.scrollHeight
      ],
      marker: [
        mnode.scrollWidth,
        mnode.scrollHeight
      ]
    }
  }

  _setSize() {
    this._size = this._getSize()
  }

  componentDidMount() {
    this._mounted = true
    this._setSize()
    this.updateSlider()
  }

  componentWillUnmount() {
    this._mounted = false
  }

  vol2styles(value=this.state.value) {
    console.log('val',value)
    let ret = {}

    // size index; if vertical take height
    // else take width
    let size_idx = this.props.vertical ? 1 : 0

    //{ width: `${this.state.value}%` }
    //{ left: `${this.state.value}%` }

    let csize = this._size.slider[size_idx]
    let msize = this._size.marker[size_idx]

    let size = Math.max(0, csize - msize)
    // percent of available slider size
    //let offset = .01 * value * size
    let offset = (value * size * 100) / 10000
    let boffset = 0
    if(this.state.buffer)
      boffset = (this.state.buffer * size * 100) / 10000

    if(this.props.vertical) {
      ret.complete = { height: offset }
      ret.marker = { bottom: offset }
      ret.buffer = { height: boffset }
    } else {
      ret.complete = { width: offset }
      ret.marker = { left: offset }
      ret.buffer = { width: boffset }
    }

    return ret
  }

  render() {

    //let styles = this.vol2styles()
    let styles = this.state.styles

    let bufferPart = !this.state.buffer ? false :
      <div
        style={ styles.buffer }
        className={ this.state.cbuffer } />


    return (
      <div
        ref='slider'
        className={ this.state.classes }
        onWheel={ this.onWheelEvt.bind(this) }>

        <div className={ this.state.cslider } />
        <div
          className={ this.state.ccomplete }
          style={ styles.complete } />
        { bufferPart }
        <a
          ref='marker'
          onDrag={ this.onDrag.bind(this) }
          onDragStart={ this.onDragStart.bind(this) }
          onDragEnd={ this.onDragEnd.bind(this) }
          className={ this.state.cmarker }
          style={ styles.marker } />
      </div>
    )

  }

  componentWillReceiveProps(next_props) {
    if(this._cooldown)
      this.updateFromPropsDebounced(next_props)
    else
      this.updateFromProps(next_props)
  }

  updateFromProps(props) {
console.log('cooldown?', this._cooldown)
    //this.setState(this.getValues(props))
  }

  onVol(value) {
    this.props.onChange( value )
  }

  updateSlider(value=this.state.value) {
    this.setState({
      value,
      styles: this.vol2styles(value)
    })
  }


  onWheelEvt(evt) {
    this._cooldown = true
    // needed because events are
    // released by react
    console.log(evt.deltaY, evt.detail, evt.deltaMode)
    this.onWheelThrottled(evt.deltaY)
  }

  onWheel(dy) {
    let val = this.state.value
    if(dy > 0) val -= 5
    if(dy < 0) val += 5
    val = Math.min(100, Math.max(0, val))
    if(val !== this.state.value) {
      this.updateSlider(val)
      this.onVolThrottled(val)
    }
    this.unsetCooldown()
  }

  onDrag(evt) {
    console.log('aaa', evt.pageX, evt.clientX)
  }

  onDragStart(evt) {
    console.log('aaa s', evt.pageX, evt.clientX)
  }

  onDragEnd(evt) {
    console.log('aaa e', evt.pageX, evt.clientX)
  }

  getValues(props=this.props) {
    let buffer = parseInt(_.get(props, 'buffer', 0)) || 0
    let value = parseInt(_.get(props, 'value', 0)) || 0
    if(isNaN(value)) value = 0
    if(isNaN(buffer)) buffer = 0
    value = Math.max(0, Math.min(100, value))
    buffer = Math.max(0, Math.min(100, buffer))

    return {
      value,
      buffer
    }
  }

  getState(props=this.props) {

    let cn = CNP(props)
    let classes = { 'slider': true, 'vertical': props.vertical }

    let cmarker = { 'marker': true }
    let cslider = { 'slider-backside': true }
    let ccomplete = { 'complete': true }
    let cbuffer = { 'buffer': true }

    if(_.get(props, 'color')) cslider[`bg-${props.color}`] = true
    if(_.get(props, 'colorMarker')) cmarker[`bg-${props.colorMarker}`] = true
    if(_.get(props, 'colorComplete')) ccomplete[`bg-${props.colorComplete}`] = true
    if(_.get(props, 'colorBuffer')) cbuffer[`bg-${props.colorBuffer}`] = true

    return {
      classes: cn(classes),
      cmarker: CN(cmarker),
      cslider: CN(cslider),
      ccomplete: CN(ccomplete),
      cbuffer: CN(cbuffer)
    }
  }

}
