'use strict'

const Log = Logger.get(__filename)

import React from 'react'
import ReactDOM from 'react-dom'

window.React = React
window.ReactDOM = ReactDOM
window.CN = require('classnames');
window.Widget = require('./widget')


// helper to get default classnames
// from the props
global.CNP = (props) => {
  let cn = _.get(props, 'className', '')
  let klass = {}
  if('string' === typeof cn) klass[cn] = true
  return obj => {
    _.merge(klass, obj)
    return CN(klass)
  }
}

const { App } = require('./view')

let subscription = null

export class UI {

  static shutdown() {
    Log.i('shutdown()', !!_.get(subscription, 'dispose'))
    if(subscription)
      subscription.dispose()
  }

  static render(target, statestream$) {

    subscription = statestream$
      .subscribe(
        state => {
          Log.d('render()#subscribe', 'got new state', state)
window.laststate = state
          ReactDOM.render(
            <App { ...state } />,
            document.getElementById(target)
          )
        }
      )
  }

}

