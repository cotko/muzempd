'use strict'

export class MPDConConf extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      conf_str: this.getConfStr(props),
      enabled: !this.shouldBeDisabled(props)
    }
  }

  getConfStr(props=this.props) {
    if(!_.get(props, 'mpd.connected'))
      return _.get(this.state, 'conf_str') || _.get(props, 'conf_str', '')
    let conconf = _.get(props, 'mpd.probe.conf')
    if(conconf.path)
      return conconf.path
    return `${conconf.host}:${conconf.port || 6600}`
  }

  shouldBeDisabled(props=this.props) {
    let connected = _.get(props, 'mpd.connected', false)
    if(connected) return true
    let probe_state = _.get(props, 'mpd.probe.state', 'STOPPED')
    return !~['FAILED', 'STOPPED'].indexOf(probe_state)
  }

  componentWillReceiveProps(next_props) {
    this.setState({
      enabled: !this.shouldBeDisabled(next_props),
      conf_str: this.getConfStr(next_props)
    })
  }

  submit() {

    let val = this.state.conf_str.trim()

    this.setState({
      enabled: false
    })

    // get the config and display it
    UTIL
    .resolveMPDConnConf(val)
    .then( conf => {
      // if socket
      if(conf.path)
        this.setState({
          conf_str: conf.path
        })
      else
        this.setState({
          conf_str: `${conf.host}:${conf.port}`
        })

      this.props.submit(conf)
    })

  }

  _onKeyDown(evt) {
    switch(evt.key) {
      case 'Enter':
        this.submit()
        break
      case 'Escape':
        this.setState({ conf_str: '' })
        break
    }
  }


  render() {
    return (
      <div
        className='form-conf'>
        <input
          type='url'
          className='input-control text full-size padding20 align-center'
          readOnly={ !this.state.enabled }
          disabled={ !this.state.enabled }
          value={ this.state.conf_str }
          onChange={ evt => this.setState({conf_str: evt.target.value}) }
          onKeyDown={ this._onKeyDown.bind(this) }
          placeholder={ __('MPD url:port or /path/to/socket') }
        />
        <div className='form-actions'>
          <button
            className='button primary'
            onClick={ this.submit.bind(this) }>
            { __('Connect to MPD') }
          </button>
        </div>
      </div>
    )
  }

}
