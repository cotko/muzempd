'use strict'

module.exports = {
  get MPDConConf() { return require('./mpdconconf').MPDConConf },
  get PlayerControls() { return require('./playercontrols').PlayerControls },
  get Playlist() { return require('./playlist').Playlist },
  get Progress() { return require('./progress').Progress }
}
