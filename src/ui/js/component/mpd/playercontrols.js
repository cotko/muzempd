'use strict'

let last_vol = 50

export class PlayerControls extends React.Component {


  requestAction(action, ctx) {
    switch(action) {
      case 'toggle-mute':
        let volume = _.get(this.props, 'state.volume', 0)
        if(volume > 0) {
          last_vol = volume
          this.props.request('service.mpd.setVol', 0)
        } else {
          this.props.request('service.mpd.setVol', last_vol || 50)
        }

        break
      case 'toggle-play':
        let state = _.get(this.props, 'state.state', 'stop')
        if(state === 'stop') {
          let songid = _.get(this.props, 'state.songid')
          if(songid !== undefined)
            this.props.request('service.mpd.playid', songid)
          else
            this.props.request('service.mpd.playpos', 0)
        } else {
          this.props.request('service.mpd.pause', state === 'play')
        }
        break
      default:
        this.props.request(`service.mpd.${action}`, ctx)
        break
    }
  }

  render() {
    let state = _.get(this.props, 'state.state', 'stop')
    let icn = state === 'play' ? 'pause' : 'play'

    let volume = _.get(this.props, 'state.volume', 0)

    let disableifstop = {}
    if(state === 'stop') disableifstop.disabled = true

    let disableifnotplaying = {}
    if(state !== 'play') disableifnotplaying.disabled = true


    return (
      <div id='player-controls'>
        <Widget.Metro.Slider

          color='amber'
          colorMarker='white'
          buffer={ 60 }
          colorBuffer='cyan'
          colorComplete='red'

          value={ volume }
          vertical={ false }
          onChange={ this.requestAction.bind(this, 'setVol') }
          />
        <Widget.Metro.TileSmall
          { ...disableifstop }
          onClick={ this.requestAction.bind(this, 'stop') }
          bg='amber'
          fg='white'
          icon='stop' />
        <Widget.Metro.TileSmall
          { ...disableifstop }
          onClick={ this.requestAction.bind(this, 'toggle-mute') }
          bg='amber'
          fg='white'
          icon={ volume > 0 ? 'volume-high' : 'volume-mute2' } />
        <Widget.Metro.TileSmall
          { ...disableifnotplaying }
          onClick={ this.requestAction.bind(this, 'seek', .1) }
          bg='amber'
          fg='white'
          icon='previous' />
        <Widget.Metro.TileSmall
          { ...disableifstop }
          onClick={ this.requestAction.bind(this, 'next') }
          bg='amber'
          fg='white'
          icon='forward' />
        <Widget.Metro.TileSmall
          { ...disableifstop }
          onClick={ this.requestAction.bind(this, 'previous') }
          bg='amber'
          fg='white'
          icon='backward' />
        <Widget.Metro.TileSmall
          onClick={ this.requestAction.bind(this, 'toggle-play') }
          bg='amber'
          fg='white'
          icon={icn} />
      </div>
    )
  }
}
