'use strict'

export class Playlist extends React.Component {

  constructor(props) {
    super(props)
    this.state = this.getState(props)
  }

  componentWillReceiveProps(next_props) {
    this.setState( this.getState(next_props) )
  }

  getState(props=this.props) {
    let list = _.get(props, 'playlist.list', [])
    let currentid = _.get(props, 'state.songid', -1)
    return {
      list,
      currentid
    }
  }

  renderList() {
    return this.state.list.map( item => {
      let name = _.chain([
        item.title,
        item.name,
        item.file
      ])
      .compact()
      .first()
      .value()

      let current = this.state.currentid === item.id

      let cs = CN({
        current
      })

      return(
        <li
          onClick={ () => {
            this.props.request('service.mpd.playid', item.id)
          }}
          key={item.id}
          className={cs}>
          { name }
        </li>
      )

    })
  }

  render() {
    //<h1 className="album-title">Scorpions - Return to Forever</h1>
    return(
      <div className='audio-player full full-height'>
        <div className='play-list-wrapper full-height'>
          <ul className='play-list full-height auto-overflow-y'>
            { this.renderList() }
          </ul>
        </div>
      </div>
    )
  }

}
