'use strict'

const style = {
  'position': 'fixed',
  'bottom': 5,
  'left': 20,
  'zIndex': 5,
  'color': 'white'
}

export class Progress extends React.Component {

  constructor(props) {
    super(props)

    this.state = this.getState(props)
  }

  componentWillReceiveProps(next_props) {
    if(_.get(next_props, 'progress$') === _.get(this, 'props.progress$'))
      return
    this.bindToProgress(next_props)
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  componentDidMount() {
    this.bindToProgress()
  }

  unsubscribe() {
    if(this._subscription)
      this._subscription.dispose()
    delete this._subscription
  }


  getState(props=this.props) {
    return {}
  }

  bindToProgress(props=this.props) {

    this.unsubscribe()

    if(!props.progress$)
      return

    this._subscription = props
    .progress$
    .map( state => {
      state.elapsed = UTIL.hhmmss(state.elapsed, true, 'h')
      state.total = UTIL.hhmmss(state.total, true, 'h')
      return state
    })
    .subscribe( state => this.setState(state) )

  }

  renderProgress() {

    return <div>
      progress {this.state.progress}% {this.state.elapsed}/{this.state.total} {this.state.state}
    </div>

  }

  renderBar() {
    if(!this.state.progress) return false
    let style = {
      width: `${this.state.progress}%`
    }
    return(
      <div
        style={ style }
        className='bar' />
    )
  }

  render() {
    return(
      <div id='progress'>
        { this.renderBar() }
        { this.renderProgress() }
      </div>
    )
  }
}
