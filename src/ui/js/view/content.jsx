'use strict'

const { MPD } = require('../component')

export class Content extends React.Component {

  constructor(props) {
    super(props)

    this.state = {}
  }

  test() {
    return(
          <div className="tile-group double" >
            <span className="tile-group-title">{ __('MPD') }</span>
            <div className="tile-container">

                <div className="tile bg-darkBlue fg-white" data-role="tile" onclick="document.location.href='http://gmail.com'" >
                    <div className="tile-content iconic">
                        <span className="icon mif-envelop"></span>
                    </div>
                    <span className="tile-label">Inbox</span>
                </div>

                <div className="tile-large bg-steel fg-white" data-role="tile" data-on-click="document.location.href='http://forecast.io'" >
                    <div className="tile-content" id="weather_bg" >
                        <div className="padding10">
                            <h1 id="weather_icon" ></h1>
                            <h1 id="city_temp"></h1>
                            <h2 id="city_name" className="text-light"></h2>
                            <h4 id="city_weather"></h4>
                            <p id="city_weather_daily"></p>

                            <p className="no-margin text-shadow">Pressure: <span className="text-bold" id="pressure"></span> mm</p>
                            <p className="no-margin text-shadow">Ozone: <span className="text-bold" id="ozone"></span></p>
                            <p className="no-margin text-shadow">Wind bearing: <span className="text-bold" id="wind_bearing"></span></p>
                            <p className="no-margin text-shadow">Wind speed: <span className="text-bold" id="wind_speed">0</span> m/s</p>
                        </div>
                    </div>
                    <span className="tile-label">Weather</span>
                </div>
            </div>
        </div>
        )
  }
  render() {
    return(
      <div id='content'>

        <MPD.PlayerControls
          {...this.props.services.mpd }
          request={ this.props.request } />

        <MPD.Progress
          {...this.props.services.mpd } />

        <div id='content-wrapper'>
            <div id='content-holder'>

            <div className='tile-area fg-white tile-area-scheme-dark'>
              { this.test() }

              <Widget.Metro.TileGroup
                label={ __('Playlist') }
                className='full-height no-overflow tile-group-inherit'
                size={ Widget.Metro.TileGroup.SIZE.FOUR } >
                <MPD.Playlist
                  {...this.props.services.mpd }
                  request={ this.props.request } />
              </Widget.Metro.TileGroup>

            </div>
          </div>
        </div>
      </div>
    )
  }
}
