'use strict'

const Log = Logger.get(__filename)

const {
  Content,
  Splash,
  Frame,
  ConnectConf
} = require('./')

export class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = this.getState(props)
  }

  getState(props=this.props) {
    return {
      ready: _.get(props, 'services.ready', false),
      show_mpd_config: _.get(props, 'services.show_mpd_config', false)
    }
  }

  componentWillMount() {
    Log.d('WILL MOUNT')
  }

  componentWillReceiveProps(next_props) {
    Log.d('will receive next props')
    this.setState(this.getState(next_props))
  }

  componentWillUnmount() {
    Log.d('WILL UNMOUNT')
  }

  onKeyDown(evt) {
    if(evt.key === 'F5')
      this.props.request('winaction', 'reload')
  }

  renderSplash() {
    return(
     <Splash
       state_msg={ __('loading..') } />
    )
  }

  renderContent() {
    return(
     <Content { ...this.props } />
    )
  }

  renderMPDConfScreen() {
    return (
      <ConnectConf
        connect= { conf => this.props.request('service.mpd.connect', conf) }
        mpd={ _.get(this.props, 'services.mpd') }
        />
    )
  }

  decideScreen() {
    if(this.state.ready)
      return this.renderContent()
    if(this.state.show_mpd_config)
      return this.renderMPDConfScreen()
    return this.renderSplash()
  }

  render() {
    return(
      <div
        id='app'
        tabIndex='1'
        className='tile-area-scheme-dark'
        onKeyDown={ this.onKeyDown.bind(this) }>
       <Frame
         request={ this.props.request } />
      { this.decideScreen() }
      </div>
    )
  }
}
