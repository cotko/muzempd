'use strict'

export class Splash extends React.Component {

  constructor(props) {
    super(props)

    this.state = {}
  }

  renderLines(num) {
    return Array(num)
      .join('.')
      .split('.')
      .map( (val, idx) => {
        let lineclass = `line line${idx+1}`
        let key = `line${idx}`
        return (
        <div className={lineclass} key={key}>
          <span className='circle circle-top' />
          <div className='dotted'>
            <span className='dot dot-top' />
            <span className='dot dot-middle-top' />
            <span className='dot dot-middle-bottom' />
            <span className='dot dot-bottom' />
          </div>
          <span className='circle circle-bottom' />
        </div>
        )
      })
  }


  render() {

    return (
    <div id='splash'>
      <div className='loading-wrapper'>
        {this.renderLines(11)}
      </div>
      <div className='loading-state'>
        {this.props.state_msg}
      </div>
    </div>
    )

  }
}
