'use strict'

module.exports = {
  get Frame() { return require('./frame').Frame },
  get Splash() { return require('./splash').Splash },
  get Content() { return require('./content').Content },
  get App() { return require('./app').App },
  get ConnectConf() { return require('./connectconf').ConnectConf }
}
