'use strict'

const { MPDConConf } = require('../component/mpd')

export class ConnectConf extends React.Component {

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <MPDConConf
        { ...this.props }
        submit={ this.props.connect }
      />
    )
  }

}
