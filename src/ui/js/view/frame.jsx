'use strict'

export class Frame extends React.Component {

  constructor(props) {
    super(props)

    this.state = {}
  }

  renderButtons() {
    const bindAction = (action) => {
      return () => {
        this.props.request('winaction', action)
      }
    }
    return(
      <div className='action-buttons'>
        <span
          className='btn close mif-cross'
          onClick={ bindAction('exit') } />
        <span
          className='btn win mif-arrow-up-right'
          onClick={ bindAction('maximize') } />
        <span
          className='btn win mif-arrow-down-left'
          onClick={ bindAction('minimize') } />
        <span
          className='btn reload mif-loop2'
          onClick={ bindAction('reload') } />
      </div>
    )
  }

  render() {
    return(
      <div id='frame'>
        { this.renderButtons() }
      </div>
    )
  }
}
