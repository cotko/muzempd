'use strict'
const _ = require('lodash')
//const ss = require('similar-songs')
const searchitunes = require('searchitunes')
const LastFmNode = require('lastfm').LastFmNode;

//ss.find({
//  artist: 'ska-p',
//  title: 'Legalizacion',
//  //title: 'Toxic',
//  //artist: 'Britney spears',
//  limit: 50, // defaults to 50
//  lastfmAPIKey: '2889d704f1f8b9b1a86a7351d918f2f0',
//  lastfmAPISecret: '7b4e1bcac08ce236b28a747e8fde3443',
//  youtubeAPIKey: 'AIzaSyDtGXt8wytiAYAJWX2rgcy_hRTJlb_eOpE',
//}, function(err, songs) {
//  console.log(err, songs); // will print out the 50 most similar tracks
//});

let _AR =  'ska-p'
let _TI =  'Legalizacion'

searchitunes({
  //country: 'US',
  //term: 'c-c music factory things that make',
  //term: 'Gramatik - Happiness On A Leash',
  //term: 'Relapso - Echo Four',
  //term: 'Carlbeats The eenergizer',
  term: `${_AR} ${_TI}`,
  //term: 'Afrolicious - Do What You Got to Do (Angel Funke Remix)',
  //term: 'Toxic Britney Spears',
  //term: 'A La mierda SKAP',
  //term: 'skap',
  //entity: 'musicArtist',
  limit: 5
}, (err, data) => {
  console.log('tole itunes', err, data)
})

//musicArtist, musicTrack, album, musicVideo, mix, song
//Please note that “musicTrack” can include both songs and music videos in the results

const lfm = new LastFmNode({
  api_key: '2889d704f1f8b9b1a86a7351d918f2f0',
  secret: '7b4e1bcac08ce236b28a747e8fde3443',
});

lfm.request(
  'track.getInfo', {
    //term: 'Carlbeats The eenergizer',
    track: _TI, // 'the eenergizer',
    artist: 'Carlbeats',
    limit: 4,
    handlers: {
      success: r => {
        console.log('lfm res', r)
      },
      error: e => {
        console.log('lfm err', e)
      }
    }
  })

//lfm.request(
//  'track.getSimilar', {
//    track: 'Do What You Got To Do',
//    artist: 'Afrolicious',
//    limit: 4,
//    handlers: {
//      success: r => {
//        console.log('lfm res', _.get(r, 'similartracks.track', r))
//      },
//      error: e => {
//        console.log('lfm err', e)
//      }
//    }
//  })
