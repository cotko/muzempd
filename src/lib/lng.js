const i18n = require('i18n')

const updateMomentLocale = () => {
  moment.locale(i18n.getLocale())
}

exports = module.exports = conf => {

  i18n.configure(conf)

  updateMomentLocale()

  return {
    i18n,
    setLocale: locale => {
      i18n.setLocale(locale)
      updateMomentLocale()

    }
  }

}
