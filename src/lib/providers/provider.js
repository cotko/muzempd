'use strict'

// base provider class

export class Provider {

  constructor(name) {
    this._name = name
  }

  static isSearchable() {
    return false
  }


}
