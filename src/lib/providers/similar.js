'use strict'

// uses last fm to get similar tracks
//

const { identifiers: { LastFM }} = require('./identify')


const getSimilarSongs = (title, artist, limit=5) => {
  return LastFM
    .search(
      LastFM.METHODS.TRACK.GET_SIMILAR, {
        artist,
        track: title,
        limit
      }
    )
    .then( normalizeSimilarSongs )
}

const getSimilarArtists = (artist, limit=5) => {
  return LastFM
    .search(
      LastFM.METHODS.ARTIST.GET_SIMILAR, {
        artist,
        limit
      }
    )
    .then( normalizeSimilarArtists )
}


const getSimilar = (title, artist, limit=5) => {

  let pprops = {
    songs: [],
    artists: getSimilarArtists(artist, limit)
  }

  if(title)
    pprops.songs = getSimilarSongs(title, artist, limit)

  return P.props(pprops)
}

const normalizeSimilarSongs = result => {
  return _
    .chain(result)
    .get('similartracks.track')
    .map( track => {

      return {

        image: LastFM.getBestImage(track.image),

        title: track.name,
        artist: track.artist.name,

        song_muzicbrainz_id: track.mbid || false,

        artist_muzicbrainz_id: track.artist.mbid || false,
        artist_url: track.artist.url
      }
    })
    .value()
}

const normalizeSimilarArtists = result => {
  return _
    .chain(result)
    .get('similarartists.artist')
    .map( artist => {
      return {
        image: LastFM.getBestImage(artist.image),
        artist: artist.name,
        artist_muzicbrainz_id: artist.mbid || false,
        artist_url: artist.url
      }
    })
    .value()
}

module.exports = {
  getSimilar,
  getSimilarSongs,
  getSimilarArtists
}
