'use strict'

const request = require('request');
const cheerio = require('cheerio');

const url = "http://www.lyrics.com/";

// Fetches from lyrics.com
// copied from https://github.com/reesdraminski/gimme-lyrics
// and refactored due to stupid design.


module.exports.INFO = {
  name: 'lyricscom',
  description: 'Lyrics from lyrics.com',
  icon: 'cloud'
}


const cleanStr = str => {
  return str
    .replace(/[+.,\/#!$%\^&\*;:{}=\-_`~()]/g, '')
    .replace(/ +/g, '-')
}

module.exports.fetch = (title, artist) => {
  return new P( (resolve, reject) => {
    try {
      artist = cleanStr(artist)
      title = cleanStr(title)
    } catch( err ) {
      return reject('INVALID_PARAMS')
    }

    let query = `${title}-lyrics-${artist}.html`

    request(`${url}${query}`, (err, res, body) => {
      if(err) return reject(err)
      if(200 !== res.statusCode)
        return reject('STATUS_CODE_NOT_OK')

      let $ = cheerio.load(body)
      let lyrics = $('#lyrics').text()

      if(_.isEmpty(lyrics))
        return reject('LYRICS_NOT_FOUND')

      // Replace "Lyrics Submitted By Text"
      lyrics = lyrics.replace(lyrics.substring(lyrics.indexOf("---")), "")

      resolve(lyrics)

    })
  })
}
