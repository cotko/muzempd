'use strict'

const request = require('request')
const qs = require('querystring')

const url = 'https://makeitpersonal.co/lyrics?'

// uses makeitpersonal.co to look up the lyrics


module.exports.INFO = {
  name: 'makeitpersonal',
  description: 'Lyrics from makeitpersonal.co',
  icon: 'cloud'
}


module.exports.fetch = (title, artist) => {

  let query = qs.stringify({artist, title})

  return new P( (resolve, reject) => {
    request(`${url}${query}`, (err, res, body) => {
      if(err) return reject(err)
      if(200 !== res.statusCode)
        return reject('STATUS_CODE_NOT_OK')

      if(`Sorry, We don't have lyrics for this song yet.` === body)
        return reject('LYRICS_NOT_FOUND')

      resolve(body)

    })
  })


}
