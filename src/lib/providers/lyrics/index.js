'use strict'

const Log = Logger.get(__filename)

const sources = {
  get makeitpersonal() { return require('./makeitpersonal') },
  get wikia() { return require('./wikia') },
  get lyricscom() { return require('./lyricscom') }
}

const list = () => {
  return _
    .reduce(sources, (memo, source, key) => {
      memo[key] = {
        lib: source,
        info: source.INFO
      }
      return memo
    }, {})
}

module.exports = {
  sources,
  list,

  // race for the first one
  fetch(title, artist) {
    return P
      .any(
        _.reduce(sources, (promises, source) => {
          promises.push(
            source
              .fetch(title, artist)
              .then( lyrics => ({
                lyrics,
                source: source.INFO
              }))
          )
          return promises
        }, [])
      )
      .catch( P.AggregateError, err => {
        Log.w('#fetch()', {artist, title}, err)
        return P.reject('LYRICS_NOT_FOUND')
      })
  }
}
