'use strict'

const illegallyrics = require('illegallyrics')


module.exports.INFO = {
  name: 'wikia',
  description: 'Lyrics from lyrics.wikia.com',
  icon: 'wikipedia'
}

module.exports.fetch = (title, artist) => {
  return new P( (resolve, reject) => {
    illegallyrics.steal(`${artist} ${title}`, (err, data) => {
      if(err) return reject(err)

      // illegallyrics makes some magic and
      // finds lyrics for practicly whatever
      // you throw it at..
      if(UTIL.cleanStrLC(artist, title)
        === UTIL.cleanStrLC(data.artist, data.title))
        return resolve(data.lyrics)

      reject('LYRICS_NOT_FOUND')

    })
  })
}
