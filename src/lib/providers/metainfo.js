'use strict'

// trys to identify the song
// from the current song in mpd
// and collects the meta info
// about it;
//   track info
//   lyrics
//   similar songs
//   ...
//

const Rx = require('rx')
const providers = require('./')

const META = {
  track: null,
  similar: null,
  lyrics: null
}

const getMetaInfo = (subject, current_song) => {

  let [artist, title] = current_song

  if(!artist && title)
    [artist, title] = title.split('[')[0].split(' - ')


  if(!title) {
    subject.onCompleted()
    return
  }


  if(!artist) artist = ''

  // try to get the info



}

const getMetaInfo$ = current_song => {
  let subject = new Rx.BehaviorSubject(
    _.merge({}, META)
  )

  setTimeout( () => {
    getMetaInfo(subject, current_song)
  }, 5)

  return subject
}

module.exports = getMetaInfo$
