'use strict'

module.exports = {
  get testInfo() { return testInfo },
  get Lyrics() { return require('./lyrics') },
  get Identify() { return require('./identify') },
  get Similar() { return require('./similar') },
  get MetaInfo() { return require('./metainfo') },
  get Search() { return require('./search') }
  //get SHCast() { return require('./shcast') },
  //get ICECast() { return require('./icecast') },
  //get Itunes() { return require('./itunes') },
  //get LastFM() { return require('./lastfm') },
  //get Youtube() { return require('./youtube') }
}


const testInfo = (title, artist) => {
  console.log('test', title, artist)

  let result = {
  }

  return module.exports
  .Identify.search(title, artist)
  .then( res => {
    result.track = res.track
    return P.all([

      module.exports.Lyrics.fetch(
        res.track.title,
        res.track.artist
      )
      .then( ly => {
        result.lyrics = ly.lyrics
      })
      .catch( err => {
        console.warn('no lyrics')
      })
      ,

      module.exports.Similar.getSimilar(
        res.track.title,
        res.track.artist
      )
      .then( res => {
        result.similar = res
      })

    ])
    .then( () => result )


  })
  .catch( err => {
    console.warn('testInfo', err)
  })
}
