'use strict'

// uses lastfm search to find the song
// info meta data

const INFO = {
  name: 'Last FM',
  description: 'Search using Last FM APIs',
  icon: 'lastfm'
}

const LastFmNode = require('lastfm').LastFmNode;
const Log = Logger.get(__filename)

const lfm = new LastFmNode({
  api_key: '2889d704f1f8b9b1a86a7351d918f2f0',
  secret: '7b4e1bcac08ce236b28a747e8fde3443',
})

const METHODS = {
  ARTIST: {
    GET_INFO: 'artist.getInfo',
    SEARCH: 'artist.search',
    GET_SIMILAR: 'artist.getSimilar',
  },
  TRACK: {
    GET_INFO: 'track.getInfo',
    GET_SIMILAR: 'track.getSimilar',
    GET_TAGS: 'track.getTags',
    SEARCH: 'track.search'
  }
}

const search = (method, params, limit=1) => {

  if(undefined === params.limit)
    params.limit = limit

  Log.i('#search()', method, params, limit)

  return new P( (resolve, reject) => {
    params.handlers = {
      success: resolve,
      error: reject
    }
    lfm.request(method, params)
  })
}

const searchTrack = (title, artist='') => {

  let params = { track: title }
  if(artist)
    params.artist = artist

  // combine track info and search
  // results
  return P
    .props({
      search: search( METHODS.TRACK.SEARCH, params )
        .then( normalizeTrackSearch )
        .catch( e => false ),
      info: search( METHODS.TRACK.GET_INFO, params )
        .then( normalizeTrackInfo )
        .catch( e => false )
    })
    .then( res => {
      let result = _.merge({}, res.info, res.search)
      if(_.isEmpty(result))
        return P.reject('NO_RESULTS')
      return result
    })
}

// normalizers
const normalizeTrackSearch = result => {
  Log.i('#normalizeTrackSearch()')
  let track = _.get(result, 'results.trackmatches.track[0]', false)
  if(_.isEmpty(track)) return P.reject('NO_RESULTS')

  return {
    image: getBestImage(track.image),
    title: track.name,
    artist: track.artist,

    // other meta
    url: track.url,
    song_muzicbrainz_id: track.mbid || false
  }
}

const normalizeTrackInfo = result => {
  Log.i('#normalizeTrackInfo()')
  let track = _.get(result, 'track', false)
  if(_.isEmpty(track)) return P.reject('NO_RESULTS')

  // genres
  let genres = _
    .chain(track)
    .get('toptags.tag')
    .castArray()
    .flatten()
    .map('name')
    .compact()
    .map(_.toLower)
    .uniq()
    .sort()
    .value()

  return {
    genres,
    title: track.name,
    artist: _.get(track, 'artist.name'),

    // other meta
    url: track.url,
    artist_muzicbrainz_id: _.get(track, 'artist.mbid', false),
    artist_url: _.get(track, 'artist.url', false)
  }

}

const getBestImage = images => {

  let img = _.find(images, {size: 'large'})
  if(img) return img['#text']
  img = _.find(images, {size: 'extralarge'})
  if(img) return img['#text']
  img = _.find(images, {size: 'medium'})
  if(img) return img['#text']

  return _
    .chain(images)
    .first()
    .get('#text')
    .value()

}

module.exports = {
  METHODS,
  INFO,
  searchTrack,
  search,
  getBestImage
}
