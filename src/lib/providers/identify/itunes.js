'use strict'

// uses itunes search to find the song
// info meta data

const INFO = {
  name: 'Itunes',
  description: 'Search using Itunes APIs',
  icon: 'apple'
}

const searchitunes = require('searchitunes')
const Log = Logger.get(__filename)

const ENTITIES = {
  ARTIST: 'musicArtist',
  TRACK: 'musicTrack',
  ALBUM: 'album',
  MUSIC_VIDEO: 'musicVideo',
  MIX: 'mix',
  SONG: 'song'
}

const search = (term, entity, limit=1) => {
  Log.i('#search()', term, entity, limit)
  return new P( (resolve, reject) => {
    searchitunes({
      entity,
      term,
      limit
    }, (err, res) => {
      if(err) return reject('NO_RESULTS')
      resolve(res)
    })
  })
}

const searchTrack = (title, artist='') => {
  let term = `${artist} ${title}`
  return search(term, ENTITIES.TRACK)
    // try different entitities
    .catch( err => {
      return search(term, ENTITIES.SONG)
    })
    .catch( err => {
      return search(term, ENTITIES.MIX)
    })
    .then( normalizeTrackInfo )
}

const normalizeTrackInfo = result => {
  let track = _.get(result, 'results[0]', false)

  if(_.isEmpty(track)) return P.reject('NO_RESULTS')


  let genres = _
    .chain(track)
    .pick([
      'primaryGenreName'
    ])
    .values()
    .compact()
    .map( _.toLower )
    .uniq()
    .sort()
    .value()

  let image = _
    .chain(track)
    .pick([
      'artworkUrl100',
      'artworkUrl60',
      'artworkUrl30'
    ])
    .values()
    .compact()
    .first()
    .value()

  return {
    genres,
    image,
    artist: track.artistName,
    title: track.trackName,
    album: track.collectionName,
    disc: track.discNumber,
    url: track.trackViewUrl || track.artistViewUrl
  }
}

module.exports = {
  INFO,
  searchTrack,
  search
}
