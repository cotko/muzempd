'use strict'

const Log = Logger.get(__filename)

const identifiers = {
  get Itunes() { return require('./itunes') },
  get LastFM() { return require('./lastfm') }
}

const list = () => {
  return _
    .reduce(identifiers, (memo, identifier, key) => {
      memo[key] = {
        lib: identifier,
        info: identifier.INFO
      }
      return memo
    }, {})
}

module.exports = {
  identifiers,
  list,

  // race for the first one
  search(title, artist) {
    return P
      .any(
        _.reduce(identifiers, (promises, identifier) => {
          promises.push(
            identifier
              .searchTrack(title, artist)
              .then( track => ({
                track,
                identifier: identifier.INFO
              }))
          )
          return promises
        }, [])
      )
      .catch( P.AggregateError, err => {
        Log.w('#search()', {artist, title}, err)
        return P.reject('NO_RESULTS')
      })
  }
}
