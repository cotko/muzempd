'use strict'

const Log = Logger.get(__filename)

const searchers = {
  get Youtube() { return require('./youtube') },
  get SoundCloud() { return require('./soundcloud') }
}

module.exports = {
  searchers
}

const list = () => {
  return _
    .reduce(searchers, (memo, searcher, key) => {
      memo[key] = {
        lib: searcher,
        info: searcher.INFO
      }
      return memo
    }, {})
}

module.exports = {
  searchers,
  list,

  // race for the first one
  search(title, artist) {
    //return P
    //  .props(
    //  )
    //return P
    //  .any(
    //    _.reduce(searchers, (promises, identifier) => {
    //      promises.push(
    //        identifier
    //          .searchTrack(title, artist)
    //          .then( track => ({
    //            track,
    //            identifier: identifier.INFO
    //          }))
    //      )
    //      return promises
    //    }, [])
    //  )
    //  .catch( P.AggregateError, err => {
    //    Log.w('#search()', {artist, title}, err)
    //    return P.reject('NO_RESULTS')
    //  })
  }
}
