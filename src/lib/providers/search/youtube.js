'use strict'

const INFO = {
  name: 'Youtube',
  description: 'Search the youtube',
  icon: 'youtube'
}

const Log = Logger.get(__filename)

const Y = require('youtube-node')
const y = new Y()

y.setKey('AIzaSyDtGXt8wytiAYAJWX2rgcy_hRTJlb_eOpE')

// music video type is supposed to be 10
// for US region. To use these two params
// we also need to set the type to `video`
y.addParam('type', 'video')
y.addParam('regionCode', 'US')
y.addParam('videoCategoryId', '10')

const search = (term, limit=5) => {
  return new P( (resolve, reject) => {
    Log.d('#search()', term, limit)
    y.search(term, limit, (err, data) => {
      if(err) return reject(err)
      resolve(data)
    })
  })
  .then( normalizeResults )
}

const related = (video_id, limit=5) => {
  Log.d('#related()', term, limit)
  return new P( (resolve, reject) => {
    y.related(video_id, limit, (err, data) => {
      if(err) return reject(err)
      resolve(data)
    })
  })
  .then( normalizeResults )
}

const normalizeResults = results => {
  return _
    .chain(results)
    .get('items')
    .map( item => {
      let res = _.pick(item.snippet, [
        'channelId',
        'channelTitle',
        'descrption',
        'title'
      ])
      res.id = item.id.videoId
      res.image = getBestImage(_.get(item, 'snippet.thumbnails'))
      return UTIL.normalizeObjectKeys(res)
    })
  .value()
}


const getBestImage = images => {
  let img =
    _.get(images, 'high') ||
    _.get(images, 'medium') ||
    _.get(images, 'default') ||
    _.first(_.values(images))

    return _.get(img, 'url', false)
}


module.exports = {
  INFO,
  search,
  related,
  getBestImage
}
