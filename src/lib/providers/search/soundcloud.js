'use strict'

const Log = Logger.get(__filename)

const INFO = {
  name: 'Soundcloud',
  description: 'Search the soundcloud',
  icon: 'soundcloud'
}

const SC = require('node-soundcloud')
SC.init({
  id: 'd5650add88e9f9b7f07ceb58f79fa665',
  secret: 'a1f6a9a925dad7bf66a46e595bde84fa'
})

const search = (term, limit=5) => {
  Log.d('#search()', term, limit)
  return new P( (resolve, reject) => {
    SC.get('/tracks', {
      q: term
    }, (err, data) => {
      if(err) return reject(err)
      resolve(data)
    })
  })
  .then( normalizeResults )
}

const normalizeResults = results => {
  return _
    .chain(results)
    .map( item => {
      let res = _.pick(item, [
        'descrption',
        'title'
      ])

      res.id = item.id
      res.url = item.permalink_url
      res.image = item.artwork_url
      res.duration = item.duration
      res.genres = _
        .chain(item.genre)
        .castArray()
        .map(_.toLower )
        .compact()
        .value()

      return res
    })
  .value()
}



module.exports = {
  INFO,
  search
}
