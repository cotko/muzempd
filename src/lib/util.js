'use strict'

const fs = require('fs')
const untildify = require('untildify')
const urlparse = require('url-parse')

const util = {

  /**
   * Tries to resolve what config is
   * passed for MPD connection via string
   * - socket
   * - or url
   */
  resolveMPDConnConf(str) {
    return P
      .resolve()
      .then( () => {
        str = str.trim()
        // return default config
        if(!str) return {
          port: 6600,
          host: 'localhost'
        }
        // check if socket
        return util
          .isSocket(util.sanitizePath(str))
          .then( err => {
            // okay
            return {
              path: str
            }
          })
          .catch( err => {
            // parse the url
            let parsed = urlparse(str)

            let httpproto = ~['http:', 'https:'].indexOf(parsed.protocol)

            // protocol is not in the string
            // meaning it was being parsed in
            // a wrong way; protocol is `host:`
            // and port is `host`
            if(!httpproto && parsed.protocol && ~str.indexOf(parsed.protocol))
              return {
                host: parsed.protocol.substring(0, parsed.protocol.length - 1),
                port: parseInt(parsed.host) || 6600
              }

            return {
              host: parsed.hostname || 'localhost',
              port: parseInt(parsed.port) || 6600
            }
          })
      })
  },


  /**
   * just replaces `~` with home dir
   */
  sanitizePath: path => {
    return untildify(path)
  },

  toIntArray: (...args) => {
    return _
      .chain(args)
      .flatten()
      .map(_.parseInt)
      .compact()
      .uniq()
      .value()
  },

  fromJSON: (val, fallback) => {
    if('string' !== typeof val) return fallback
    let ret = val
    try {
      ret = JSON.parse(val)
    } catch (e) {
      if(fallback !== undefined)
        ret = fallback
    }
    return ret
  },

  fileStats: (uri) => {
    return new P( (res, rej) => {
      fs.lstat(uri, (err, stats) => {
        if(err) return rej(err)
        res(stats)
      })
    })
  },

  isDir: (uri) => {
    return util
      .fileStats(uri)
      .then((stats) => stats.isDirectory())
  },

  isFile: (uri) => {
    return util
      .fileStats(uri)
      .then((stats) => stats.isFile())
  },

  isSocket: (uri) => {
    return util
      .fileStats(uri)
      .then((stats) => stats.isSocket())
  },

  isSymLink: (uri) => {
    return util
      .fileStats(uri)
      .then((stats) => stats.isSymbolicLink())
  },

  /**
   * Takes seconds and reutrns [
   *  h,
   *  m,
   *  s
   * ]
   *
   */
  seconds2time(seconds) {
    let m = Math.floor(seconds / 60)
    seconds = seconds%60
    let h = Math.floor(m / 60)
    m = m%60
    return [h, m, seconds]
  },

  /**
   * Convert seconds to hh:mm:ss
   * or h:m:s in case @param pad = false
   * @param skipzeroes can be boolean
   *        or a string from where on
   *        to skip the zeroes, can be
   *        'm' or 'h'
   */
  hhmmss(time, pad=false, skipzeroes=false) {
    if('string' === typeof time)
      time = util.seconds2time(parseInt(time) || 0)
    if('number' === typeof time)
      time = util.seconds2time(time)

    if(skipzeroes) {
      if(!time[0]) {
        time.splice(0, 1)
        // check minutes
        if(skipzeroes !== 'h' && !time[0]) time.splice(0, 1)
      }
    }

    if(pad) time = time.map( v => v < 10 ? `0${v}` : v )

    return time.join(':')
  },

  /**
   * Cleans the string from non alphanum
   * characters
   */
  cleanStr(...args) {
    return _
      .chain(args)
      .flatten()
      .map(_.toString)
      .map( v => v.replace(/[^\w]/g, '') )
      .join('')
      .value()
  },

  /**
   * Same as above but to lower case
   */
  cleanStrLC(...args) {
    return util
      .cleanStr
      .apply(util, args)
      .toLowerCase()
  },

  /**
   * All keys are lower case
   * all non alpha chars are replaced
   * with `_`
   */
  normalizeObjectKeys(obj) {
    for(let key in obj) {
      let val = obj[key]
      delete obj[key]
      key = key
        .toLowerCase()
        .replace(/[^a-z_]/g, '_')
      obj[key] = val
    }

    return obj
  }

}

exports = module.exports = util
