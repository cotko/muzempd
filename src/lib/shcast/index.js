'use strict'

const qs = require('querystring')
const request = require('request')
const xml2js = require('xml2js')

const Log = Logger.get(__filename)

const KEY = 'fa1669MuiRPorUBw'
const URL_SEARCH = 'http://api.shoutcast.com/legacy/genrelist'
const URL_TUNE_IN = 'http://yp.shoutcast.com/sbin/tunein-station.m3u'

const getUrl = (url, params) => {
  return `${url}?${qs.encode(params)}`
}

const search = text => {

  return new P( (resolve, reject) => {

    request(
      getUrl(URL_SEARCH, {
        k: KEY,
        search: text
      }), (err, res, body) => {

        if(err) {
          Log.e('#search() request', err)
          return reject(err)
        }

        if(200 !== res.statusCode) {
          Log.e('#search() status code not ok', res.statusCode)
          return reject('STATUS_CODE_NOT_OK')
        }

        xml2js.parseString(body, (err, data) => {
          if(err) {
            Log.e('#search() xml parse error', err)
            return reject('XML_PARSE_ERROR')
          }
          resolve(data)
        })
      })
  })
  .then( parseSearch )
}

const parseSearch = results => {
  let ret = []
  let { stationlist: { station: stations, tunein } } = results

  for(let { $: station } of stations) {

    //"$": {
    //  "name": "Kosava Cafe",
    //  "mt": "audio/mpeg",
    //  "id": "1234760",
    //  "br": "48",
    //  "genre": "Pop",
    //  "ct": "Cafe del Mar (CD Series) - Ibiza, Vol. 2 - Deadbeats - Feel Good",
    //  "lc": "0"
    //}

    station.media_type = station.mt
    station.bitrate = parseInt(station.br) || 'N/A'
    station.title = station.ct
    station.listeners_count = parseInt(station.ct) || 0
    station.file = getUrl(URL_TUNE_IN, {
      id: station.id
    })
    station.genre = _
      .chain(station.genre)
      .castArray()
      .map( _.toLower )
      .compact()
      .value()

    // adding the station to the playlist
    // should be done via MPD.api.Playlists.load()
    station.mpd = {
      method: 'loadplaylist'
    }

    ret.push(_.omit(station, [
      'mt',
      'br',
      'ct',
      'lc'
    ]))

  }

  return ret
}

module.exports = {
  search
}
