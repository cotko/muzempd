'use strict'


const lib = {
  get Logger() { return require('./logger').Logger },
  get Util() { return require('./util') },
  get Lng() { return require('./lng') },
  get Err() { return require('./err').Err },
  get MPD() { return require('./mpd') },
  get SHCast() { return require('./shcast') }
}

module.exports = lib

global.LIB = lib

// expose err for all the libs
global.ERR = lib.Err
