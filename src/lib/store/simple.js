'use strict'

// local storage

class Store {

  constructor(prefix) {
    if('string' === typeof prefix && prefix.length)
      this.prefix = `${prefix}#`
    else
      this.prefix = ''
  }

  _2key(key) {
    return `${this.prefix}${key}`
  }

  set(key, val) {
    if('string' !== typeof val)
      val = JSON.stringify(val)
    localStorage.setItem(this._2key(key), val)
  }

  get(key, fallback) {
    return UTIL.fromJSON(
      localStorage.getItem(this._2key(key)),
      fallback
    )
  }

  del(key) {
    return localStorage.removeItem(this._2key(key))
  }


}

const global_store = new Store('global')

exports = module.exports = {

  Store: Store,

  set: global_store.set.bind(global_store),
  get: global_store.get.bind(global_store),
  del: global_store.del.bind(global_store),

  clearAll: () => { localStorage.clear() }
}
