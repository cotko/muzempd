'use strict'

const path = require('path')

const getTagFromFilename = (filepath) => {
  return path
    .relative(__filename, filepath)
    .replace(/[\.\./]+/, '')
    .replace(/\.js$/, '')
    .replace(/[/]/g, ':')
    .toUpperCase()
}

// simple logger

export class Logger {

  constructor(tag) {
    this.tag = tag
    this.enable()
  }

  /**
   * creates the TAG from a file loc
   */
  static get(filepath) {
    return new Logger(getTagFromFilename(filepath))
  }

  enable() {
    this._enabled = true
    return this
  }

  disable() {
    this._enabled = false
    return this
  }

  _getArgs(args) {
    args.unshift(this.tag)
    return args
  }

  d(...args) {
    if(!this._enabled) return
    Logger.d.apply(Logger, this._getArgs(args))
  }

  l(...args) {
    if(!this._enabled) return
    Logger.l.apply(Logger, this._getArgs(args))
  }

  i(...args) {
    if(!this._enabled) return
    Logger.i.apply(Logger, this._getArgs(args))
  }

  w(...args) {
    if(!this._enabled) return
    Logger.w.apply(Logger, this._getArgs(args))
  }

  e(...args) {
    if(!this._enabled) return
    Logger.e.apply(Logger, this._getArgs(args))
  }

  static d(tag, ...args) {
    args.unshift(tag)
    console.debug.apply(console, args)
  }

  static l(tag, ...args) {
    args.unshift(tag)
    console.log.apply(console, args)
  }

  static i(tag, ...args) {
    args.unshift(tag)
    console.info.apply(console, args)
  }

  static w(tag, ...args) {
    args.unshift(tag)
    console.warn.apply(console, args)
  }

  static e(tag, ...args) {
    args.unshift(tag)
    console.error.apply(console, args)
  }

}
