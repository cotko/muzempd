exports = module.exports = {

  ERR_NOT_CONNECTED_TO_MPD: {
    msg: 'Not connected to MPD daemon.'
  }

}
