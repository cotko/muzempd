'use stict'

const Log = Logger.get(__filename)

ERR.registerErrors(require('./errs'))

const mpd = require('mpd')
const discover = require('./discover')
const api = require('./api')
const SimpleStore = require('../store').Simple.Store
const RxStreams = require('./rx')

const store = new SimpleStore('mpd')

let AUTO_CONNECT_RETRY_CNT = 0
let RETRY_UNTIL_CONNECTED_TID = false

export class MPD {

  static get AUTO_CONNECT_RETRY_CNT() {
    return AUTO_CONNECT_RETRY_CNT
  }

  static get parseArrayMessage() {
    return this.api.Parsers.parseArrayMessage
  }

  static get parseKeyValueMessage() {
    return this.api.Parsers.parseKeyValueMessage
  }

  static get store() {
    return store
  }

  static get api() {
    return api
  }

  static get probe() {
    return discover
  }

  /**
   * Rx streams
   */
  static get $() {
    return RxStreams
  }

  /**
   * accepts connection configs
   * in case auto discovery doen't
   * find out the usual ones
   */
  static connect(connection_configs) {
    Log.i('connect()')
    return P
      .resolve()
      .then( () => {

        if(this.api.Instance.isConnected)
          return true

        return this
          .probe
          .discover(connection_configs)
          .then( conf => this.api.Instance.connect(conf) )
          .then( () => {
            // manage events via rx
            this.$.Instance.handleInstance( this.api.Instance.instance )
            return
          })
      })
  }

  static disconnect() {
    Log.i('disconnect()')
    return P
      .resolve()
      .then( () => {
        return this.api.Instance.disconnect()
      })
  }

  static dispose() {
    Log.i('dispose()')
    return this
      .disconnect()
      .then( () => {
        Log.i('dispose()#stream')
        return this.$.Stream.shutdown()
      })
  }

  /**
   * Retries connecting untill connected
   */
  static retryUntillConnected(timeout) {
    timeout = Math.max(4000, parseInt(timeout) || 4000)
    Log.i('retryUntillConnected() timeout:', timeout)

    RETRY_UNTIL_CONNECTED_TID = true

    this
      .connect()
      .then( () => {

        RETRY_UNTIL_CONNECTED_TID = false

        AUTO_CONNECT_RETRY_CNT = 0

        this
          .$
          .Discover
          .publishAutoDiscovery(false, AUTO_CONNECT_RETRY_CNT)

      })
      .catch( err => {

        if(RETRY_UNTIL_CONNECTED_TID === false)
          return

        AUTO_CONNECT_RETRY_CNT++

        this
          .$
          .Discover
          .publishAutoDiscovery(true, AUTO_CONNECT_RETRY_CNT)

        Log.i('retryUntillConnected() fail, scheduling new try..')

        setTimeout( () => {
          this.retryUntillConnected(timeout)
        }, timeout)

      })

  }


  static stopRetryUntillConnected() {
    clearTimeout(RETRY_UNTIL_CONNECTED_TID)
    RETRY_UNTIL_CONNECTED_TID = false
    this
      .$
      .Discover
      .publishAutoDiscovery(true, AUTO_CONNECT_RETRY_CNT)

  }


}

