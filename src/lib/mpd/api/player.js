'use strict'

// player interface
// https://www.musicpd.org/doc/protocol/playback_option_commands.html
// https://www.musicpd.org/doc/protocol/playback_commands.html

const {
  Instance,
  Parsers
} = require('.')


/**
 * Set consume to on/off
 */
exports.consume = bool => {
  bool = Parsers.parseBool(bool) ? 1 : 0
  return Instance
    .sendCommand( Instance.cmd('consume', [bool]) )
}

exports.crossfade = seconds => {
  return Instance
    .sendCommand( Instance.cmd('crossfade', [Parsers.parseInt(seconds) | 0]) )
}

exports.random = bool => {
  bool = Parsers.parseBool(bool) ? 1 : 0
  return Instance
    .sendCommand( Instance.cmd('random', [bool]) )
}

exports.repeat = bool => {
  bool = Parsers.parseBool(bool) ? 1 : 0
  return Instance
    .sendCommand( Instance.cmd('repeat', [bool]) )
}

exports.single = bool => {
  bool = Parsers.parseBool(bool) ? 1 : 0
  return Instance
    .sendCommand( Instance.cmd('single', [bool]) )
}

exports.setVol = vol => {
  vol = Math.max(0, Math.min(100, Parsers.parseInt(vol) || 0))
  return Instance
    .sendCommand( Instance.cmd('setvol', [vol]) )
}

exports.replayGainStatus = () => {
  return Instance
    .sendCommand('replay_gain_status', [
      Parsers.parseKeyValueMessage
    ])
}

exports.replayGainMode = mode => {

  if(!REPLAY_GAIN_MODES_REVERSED[mode])
    mode = exports.REPLAY_GAIN_MODES.OFF

  return Instance
    .sendCommand( Instance.cmd('replay_gain_mode', [mode]) )
}


/**
 * Playback ctl
 **/

exports.playpos = song_pos => {
  song_pos = Parsers.parseInt(song_pos) || 0
  return Instance
    .sendCommand( Instance.cmd('play', [song_pos]) )
}

exports.playid = song_id => {
  song_id = Parsers.parseInt(song_id) || 0
  return Instance
    .sendCommand( Instance.cmd('playid', [song_id]) )
}

exports.pause = pause => {
  pause = Parsers.parseBool(pause) ? 1 : 0
  return Instance
    .sendCommand( Instance.cmd('pause', [pause]) )
}

exports.stop = () => {
  return Instance.sendCommand('stop')
}

exports.previous = () => {
  return Instance.sendCommand('previous')
}

exports.next = () => {
  return Instance.sendCommand('next')
}

/**
 * Seek current song
 * @time is seconds (can have fractions)
 */
exports.seek = time => {
  return Instance.sendCommand(
    Instance.cmd('seekcur', [
      Parsers.parseFloat(time)
    ])
  )
}

/**
 * Play song at @song_pos from @time
 */
exports.seekpos = (song_pos, time) => {
  return Instance.sendCommand(
    Instance.cmd('seek', [
      Parsers.parseInt(song_pos) || 0,
      Parsers.parseFloat(time)
    ])
  )
}

/**
 * Play @song_id from @time
 */
exports.seekid = (song_id, time) => {
  return Instance.sendCommand(
    Instance.cmd('seekid', [
      Parsers.parseInt(song_id) || 0,
      Parsers.parseFloat(time)
    ])
  )
}


exports.REPLAY_GAIN_MODES = {
  OFF: 'off',
  TRACK: 'track',
  ALBUM: 'album',
  AUTO: 'auto'
}

const REPLAY_GAIN_MODES_REVERSED = _.invert(exports.REPLAY_GAIN_MODES)
