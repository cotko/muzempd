'use strict'

// Stored playlists
// https://www.musicpd.org/doc/protocol/playlist_files.html

const {
  Instance,
  Parsers
} = require('.')

const util = require('../util')

exports.list = () => {
  return Instance
    .sendCommand(
      'listplaylists',
      Parsers.parseArrayMessage
    )
    .map( list => {
      list.last_modified = moment(list['Last-Modified']).toDate()
      delete list['Last-Modified']
      return list
    })
}

exports.listinfo = playlist => {
  return Instance
    .sendCommand(
      Instance.cmd(
        'listplaylistinfo',
        [playlist]
      ),
      Parsers.parseArrayMessage
    )
    .map( list => {

      list = util.normalizeObjectKeys(item)

      // add missing name and time for
      // streams/playlist items
      if(!list.name)
        list.name = Parsers.url2name(list.file, true)
      if(!list.time)
        list.time = 'N/A'

      return list
    })
}

/**
 * Load playlist into current queue
 * @param playlist is playlist's name
 * @params from, to optional start - to
 *          part of the list to load
 */
exports.load = (playlist, from, to) => {
  if(undefined !== from) from = Parsers.parseInt(from)
  if(undefined !== to) to = Parsers.parseInt(to)
  let args = [playlist]
  if(from >= 0 || to > from)
    args.push(`${from}:${to}`)
  console.log('argssss', args)
  return Instance
    .sendCommand( Instance.cmd('load', args) )
}

/**
 * Adds uri to playlist.m3u
 * if playlist playlist.m3u does not exist
 * it will be created
 */
exports.add = (playlist, uri) => {
  return Instance
    .sendCommand( Instance.cmd('playlistadd', [
      playlist,
      uri
    ]))
}

/**
 * Clears the playlist.m3u playlist
 */
exports.clear = playlist => {
  return Instance
    .sendCommand( Instance.cmd('playlistclear', [
      playlist
    ]))
}

/**
 * Deletes song at position from the playlist
 */
exports.deleteSongAt = (playlist, song_pos) => {
  return Instance
    .sendCommand( Instance.cmd('playlistdelete', [
      playlist,
      song_pos
    ]))
}

/**
 * Moves song at position to new position
 * for given playlist
 */
exports.moveSongAt = (playlist, song_pos, to_pos) => {
  return Instance
    .sendCommand( Instance.cmd('playlistmove', [
      playlist,
      song_pos,
      to_pos
    ]))
}

/**
 * Renames the playlist
 */
exports.rename = (playlist, newname) => {
  return Instance
    .sendCommand( Instance.cmd('rename', [
      playlist,
      newname,
    ]))
}

/**
 * Deletes the playlist from the playlist dir
 */
exports.remove = (playlist) => {
  return Instance
    .sendCommand( Instance.cmd('rm', [
      playlist
    ]))
}

/**
 * Saves current playlist as a new playlist
 * to the playlist directory
 */
exports.save = playlistname => {
  return Instance
    .sendCommand( Instance.cmd('save', [
      playlistname
    ]))
}
