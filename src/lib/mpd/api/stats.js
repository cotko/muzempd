'use strict'


// Meaning of values:
// https://www.musicpd.org/doc/protocol/command_reference.html#status_commands

const {
  Instance,
  Parsers
} = require('.')



const statsParser = Parsers.makeParser({
  // number of artists
  artists: Parsers.parseInt,

  // number of albums
  albums: Parsers.parseInt,

  // number of songs
  songs: Parsers.parseInt,

  // daemon uptime in seconds
  uptime: Parsers.parseInt,

  // sum of all song times in the db
  db_playtime: Parsers.parseInt,

  // last db update in UNIX time
  db_update: Parsers.parseInt,

  // time length of music played
  playtime: Parsers.parseInt
})


exports.get = () => {
  return Instance
    .sendCommand(
      'stats',
      [
        Parsers.parseKeyValueMessage,
        statsParser
      ]
    )
    .then( data => {

      let now = new Date()
      let uptime = moment(data.uptime * 1000)
      let uptime_from = new Date(now - uptime.toDate())

      data.uptime_from = uptime_from
      data.fmt_uptime = moment.duration(data.uptime * 1000).humanize()


      data.fmt_playtime = moment.duration(data.playtime * 1000).humanize()
      data.fmt_db_playtime = moment.duration(data.db_playtime * 1000).humanize()

      data.db_update_date = moment(data.db_update * 1000).toDate()
      data.fmt_db_update = moment.duration((data.db_update * 1000) - now).humanize(true)

      return data

    })
}
