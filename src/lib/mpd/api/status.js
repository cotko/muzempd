'use strict'

// Meaning of values:
// https://www.musicpd.org/doc/protocol/command_reference.html#status_commands

const {
  Instance,
  Parsers,
  Playlist
} = require('.')



const statusParser = Parsers.makeParser({
  volume: Parsers.parseInt,
  songid: Parsers.parseInt,
  nextsongid: Parsers.parseInt,
  playlistlength: Parsers.parseInt,
  playlist: Parsers.parseInt,
  song: Parsers.parseInt,
  nextsong: Parsers.parseInt,
  bitrate: Parsers.parseInt,
  updating_db: Parsers.parseInt,

  elapsed: Parsers.parseFloat,
  mixrampdb: Parsers.parseFloat,

  repeat: Parsers.parseBool,
  random: Parsers.parseBool,
  single: Parsers.parseBool,
  consume: Parsers.parseBool,

  time: val => {
    return _
      .chain(val)
      .split(':')
      .map( v => Parsers.parseInt(v) || 0 )
      .reduce( (memo, val, pos) => {
        if(0 === pos) memo.elapsed = val
        else memo.total = val
        return memo
      }, {})
      .value()

  },

  audio: val => {
    return _
      .chain(val)
      .split(':')
      .map( v => Parsers.parseInt(v) || 0 )
      .reduce( (memo, val, pos) => {
        if(0 === pos) memo.sample_rate = val
        else if(1 === pos) memo.bits = val
        else if(2 === pos) memo.channels = val
        return memo
      }, {})
      .value()

  }

})


exports.get = () => {
  return Instance
    .sendCommand(
      'status',
      [
        Parsers.parseKeyValueMessage,
        statusParser
      ]
    )
    .then( data => {
      data.is_updating = !!data.updating_db
      return data
    })
}

/**
 * Clears the current error
 */
exports.clearerror = () => {
  return Instance.sendCommand('clearerror')
}

exports.currentsong = () => {
  return Instance.sendCommand('currentsong', [
    Parsers.parseArrayMessage,
    Parsers.songArrayParser
  ])
  .then( a => _.isEmpty(a[0]) ? null : Playlist.listItemTransform(a[0]) )
}

