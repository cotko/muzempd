'use strict'

module.exports = {
  get Playlist() { return require('./playlist') },
  get Playlists() { return require('./playlists') },
  get Status() { return require('./status') },
  get Stats() { return require('./stats') },
  get Player() { return require('./player') },
  get Instance() { return require('./instance') },
  get Tags() { return require('./tags') },
  get Parsers() { return require('./parsers') },
  get Database() { return require('./database') }
}
