'use strict'

const mpd = require('mpd')

const parseDomain = require('parse-domain')
const Url = require('url')

const iface = {

  get parseArrayMessage() {
    return mpd.parseArrayMessage
  },

  get parseKeyValueMessage() {
    return mpd.parseKeyValueMessage
  },

  parseInt(num) {
    return _.parseInt(num)
  },

  parseFloat(num) {
    let val = parseFloat(num)
    if(isNaN(val)) return 0
    return val
  },

  parseBool(val) {
    if(true === val || false === val)
      return val

    if(1 === val) return true
    if(0 === val) return false

    if('string' === typeof val) {
      val = val.toLowerCase().trim()
      if('true' === val) return true
      if('1' === val) return true
      if('on' === val) return true
      return false
    }
  },

  parseDate(val) {
    return moment(val).toDate()
  },

  /**
   * Makes a parser
   * @param parser object of key=>parserFn values
   *        each key if found will be passed to
   *        the mapped parser
   */
  makeParser(parser) {
    return data => {
      for(let key in data)
        if(_.isFunction(parser[key]))
          data[key] = parser[key](data[key])
      return data
    }
  },

  makeArrayParser(parser) {
    if(!_.isFunction(parser))
      parser = this.makeParser(parser)
    return data => {
      return data.map(parser)
    }
  },

  /**
   * Parses the url
   * @returns {
   *  domain,
   *  subdomain,
   *  tld (.com etc..)
   *  url props...
   * }
   */
  parseUrl(url) {
    return _.merge(
      Url.parse(url),
      parseDomain(url)
    )
  },

  /**
   * Parses the url and joins domain and subdomain
   * as name
   * @param smartass
   *    if true then try to swap domain and
   *    subdomain in case one of them seems
   *    to not contain name info
   */
  url2name(url, smartass) {
    let parsed = this.parseUrl(url)
    let {path, domain, subdomain} = parsed
    if(smartass && subdomain) {
      // simple; try to guess if
      // domain is informative
      for(let di of DOMAIN_INFO_WORDS) {
        if(~domain.indexOf(di)) {
          [domain, subdomain] = [subdomain, domain]
          break
        }
      }
    }

    if(path)
      path = `${path.replace(/\//g, ' ')}`.trim()

    if(subdomain)
      subdomain = `(${subdomain})`

    return _.chain([
      domain,
      path,
      subdomain
    ])
    .filter( a => !!a )
    .join(' ')
    .value()
  },

  /**
   * Number between 0 and 255
   */
  parsePriority(prio) {
    prio = this.parseInt(prio) || 0
    return Math.max(0, Math.min(prio, 255))
  },

  get songParser() {
    return songParser
  },

  get songArrayParser() {
    return songArrayParser
  }

}

const songParser = iface.makeParser({
  'Last-Modified': iface.parseDate,
  Id: iface.parseInt,
  Pos: iface.parseInt,
  Time: iface.parseInt,
  Prio: iface.parseInt
})

const songArrayParser = iface.makeArrayParser(songParser)


const DOMAIN_INFO_WORDS = [
  'streaming',
  'stream',
  'live',
  'playback',
  'playing'
]

module.exports = iface
