'use strict'

// MPD database interface
// https://www.musicpd.org/doc/protocol/database.html


const {
  Instance,
  Parsers,
  Tags
} = require('.')

const util = require('../util')



exports.find = (what, tag, from, to) => {

  let args = util.fromToArgs(from, to, true)

  args.unshift(
    Tags.validateWithFallback(tag),
    util.assureString(what)
  )

  return Instance
    .sendCommand( Instance.cmd('find', args), [
      Parsers.parseArrayMessage,
      Parsers.songArrayParser
    ])
    .map( util.normalizeObjectKeys )
    .filter( _.negate(_.isEmpty) )
}

exports.search = (what, tag, from, to) => {

  // if tag is undefined
  // then search for file and any
  // which will kind of search for everything.
  if(tag === undefined)
    tag = [
      Tags.ANY,
      Tags.FILE
    ]

  // handle multiple queries if more
  // than one tags are used
  if(_.isArray(tag) && !_.isEmpty(tag)) {
    let promises = []
    for(let tg of tag)
      promises.push(exports.search(
        what,
        tg,
        from,
        to
      ))

    return P
      .all(promises)
      // flatten all args into array
      .then( _.flatten )
      // use file as uniq identifier
      .reduce( (memo, item) => {
        memo[item.file] = item
        return memo
      }, {})
      // turn object back into array
      .then( _.values )
  }


  let args = util.fromToArgs(from, to, true)

  args.unshift(
    Tags.validateWithFallback(tag),
    util.assureString(what)
  )

  return Instance
    .sendCommand( Instance.cmd('search', args), [
      Parsers.parseArrayMessage,
      Parsers.songArrayParser
    ])
    .map( util.normalizeObjectKeys )
    .filter( _.negate(_.isEmpty) )
}

/**
 * Lists items by tag
 * @param tag required
 * @param filter_type is a tag to additionaly filter by
 * @param needle if filter_type is present,
 *               filter by this string
 */
exports.list = (tag, filter_type, needle) => {

  let args = [tag]

  if(Tags.isValid(filter_type)) {
    args.push(filter_type)
    args.push(util.assureString(needle))
  }

  return Instance
    .sendCommand(
      Instance.cmd('list', args), [
        Parsers.parseArrayMessage
      ]
    )
    .map( util.normalizeObjectKeys )
}

// shorthands for listing
// various tags
const makeLister = (tag, filter_type) => {
  return (needle, filter_type_override) => {
    let ft = filter_type_override || filter_type
    return exports
      .list(tag, ft, needle)
      .reduce( (memo, item) => {
        memo.push(item[tag])
        return memo
      }, [])
  }
}

exports.listArtists = makeLister(Tags.ARTIST)
exports.listTitles = makeLister(Tags.TITLE)
exports.listFiles = makeLister(Tags.FILE)
exports.listAlbums = makeLister(Tags.ALBUM)
exports.listGenres = makeLister(Tags.GENRE)
exports.listComposers = makeLister(Tags.COMPOSER)

/**
 * Lists albums from artist
 * Expects artist name as a param
 */
exports.listArtistAlbums = makeLister(Tags.ALBUM, Tags.ARTIST)

/**
 * Lists song titles from artist
 */
exports.listArtistTitles = makeLister(Tags.TITLE, Tags.ARTIST)
