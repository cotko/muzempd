'use strict'

// MPD supported tags for various commands
// https://www.musicpd.org/doc/protocol/tags.html

const TAGS = {
  // the artist name. Its meaning is not well-defined;
  // see composer and performer for more specific tags.
  ARTIST: 'artist',

  // same as artist, but for sorting.
  // This usually omits prefixes such as "The".
  ARTISTSORT: 'artistsort',

  // the album name.
  ALBUM: 'album',

  // same as album, but for sorting.
  ALBUMSORT: 'albumsort',

  // on multi-artist albums, this is the artist name
  // which shall be used for the whole album.
  // The exact meaning of this tag is not well-defined.
  ALBUMARTIST: 'albumartist',

  // same as albumartist, but for sorting.
  ALBUMARTISTSORT: 'albumartistsort',

  // the song title.
  TITLE: 'title',

  // the track number within the album.
  TRACK: 'track',

  // a name for this song. This is not the song title.
  // The exact meaning of this tag is not well-defined.
  // It is often used by badly configured internet radio
  // stations with broken tags to squeeze both the artist
  // name and the song title in one tag.
  NAME: 'name',

  // the music genre.
  GENRE: 'genre',

  // the song's release date.
  // This is usually a 4-digit year.
  DATE: 'date',

  // the artist who composed the song.
  COMPOSER: 'composer',

  // the artist who performed the song.
  PERFORMER: 'performer',

  // a human-readable comment about this song.
  // The exact meaning of this tag is not well-defined.
  COMMENT: 'comment',

  // the disc number in a multi-disc album.
  DISC: 'disc',

  // the artist id in the MusicBrainz database.
  MUSICBRAINZ_ARTISTID: 'musicbrainz_artistid',

  // the album id in the MusicBrainz database.
  MUSICBRAINZ_ALBUMID: 'musicbrainz_albumid',

  // the album artist id in the MusicBrainz database.
  MUSICBRAINZ_ALBUMARTISTID: 'musicbrainz_albumartistid',

  // the track id in the MusicBrainz database.
  MUSICBRAINZ_TRACKID: 'musicbrainz_trackid',

  // the release track id in the MusicBrainz database.
  MUSICBRAINZ_RELEASETRACKID: 'musicbrainz_releasetrackid',

  FILE: 'file',

  ANY: 'any'

}



const iface = _.merge({

  /**
   * Special type of a tag which is also valid
   */
  get MODIFIED_SINCE() {
    return 'modified-since'
  },

  isValid(tag) {
    if(TAGS_REVERSED[tag]) return true
    if(this.MODIFIED_SINCE === tag) return true
    if(_.isString(tag)) {
      let tags = tag.split(',')
      for(let tg of tags)
        if(!TAGS_REVERSED[tg]) return false
      return true
    }
    return false
  },

  /**
   * combines tags passed via args
   * could be each arg or arrays
   * validates them and keeps only
   * valid ones
   */
  combine() {
    return _.chain(arguments)
      .flatten()
      .split(/[,|]/g)
      .map(_.trim)
      .compact()
      .uniq()
      .filter( tag => this.isValid(tag) )
      .join(',')
      .value()
  },

  validateWithFallback(tag, fallback) {
    if(!this.isValid(tag)) {
      if(this.isValid(fallback)) return fallback
      return TAGS.ANY
    }
    return tag
  },

  get DEFAULT_TAGS() {
    return DEFAULT_TAGS
  }

}, TAGS)

const TAGS_REVERSED = _.invert(TAGS)
const DEFAULT_TAGS = iface.combine(
  TAGS.TITLE,
  TAGS.ARTIST,
  TAGS.ALBUM,
  TAGS.NAME,
  TAGS.COMPOSER,
  TAGS.PERFORMER
)

module.exports = iface
