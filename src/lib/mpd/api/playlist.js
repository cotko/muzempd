'use strict'

// Current playlist
// https://www.musicpd.org/doc/protocol/queue.html


const {
  Instance,
  Parsers,
  Tags
} = require('.')

const util = require('../util')

const listItemTransform = item => {
  item = util.normalizeObjectKeys(item)

  // add missing name and time for
  // streams/playlist items
  if(!item.name) {
    if(item.file)
      item.name = Parsers.url2name(item.file, true)
    else
      item.name = 'N/A'
  }
  if(!item.time)
    item.time = 'N/A'


  return item
}

exports.listItemTransform = listItemTransform


/**
 * Add uri to the current playlist
 */
exports.add = uri => {
  return Instance
    .sendCommand( Instance.cmd('add', [
      uri
    ]))
}

/**
 * Add uri to the playlist at the position
 */
exports.addid = (uri, pos) => {
  return Instance
    .sendCommand( Instance.cmd('addid', [
      uri,
      Parsers.parseInt(pos)
    ]), [
      Parsers.parseKeyValueMessage,
      Parsers.songParser
    ])
    .then( util.normalizeObjectKeys )
}

/**
 * Clear current playlist
 */
exports.clear = () => {
  return Instance
    .sendCommand('clear')
}

/**
 * Get playlist info
 */
exports.list = (from, to) => {
  let args = util.fromToArgs(from, to)

  return Instance
    .sendCommand( Instance.cmd('playlistinfo', args), [
      Parsers.parseArrayMessage,
      Parsers.songArrayParser
    ])
    .map( listItemTransform )
}

/**
 * Similar to the list but takes
 * optional song ids
 */
exports.listid = (...songids) => {
  let args = UTIL.toIntArray(songids)
  return Instance.sendCommand( Instance.cmd('playlistid', args), [
    Parsers.parseArrayMessage,
    Parsers.songArrayParser
  ])
  .map( listItemTransform )
}

/**
 * Deletes song at position from
 * or range of songs from - to if to
 * is passed
 */
exports.delete = (from, to) => {
  let args = util.fromToArgs(from, to)

  return Instance
    .sendCommand( Instance.cmd('delete', args) )

}

/**
 * Removes song by id from playlist
 */
exports.deleteid = songid => {
  return Instance
    .sendCommand( Instance.cmd('deleteid', [
      Parsers.parseInt(songid)
    ]))
}

/**
 * Moves song at position from or range from-to
 * to the new position
 */
exports.move = (from, to, pos) => {
  let args = util.fromToArgs(from, to)
  args.push( Parsers.parseInt(pos) || 0 )
  return Instance
    .sendCommand( Instance.cmd('move', args) )
}

/**
 * Moves song by id to the position
 */
exports.moveid = (songid, pos) => {
  return Instance
    .sendCommand( Instance.cmd('moveid', [
      Parsers.parseInt(songid),
      Parsers.parseInt(pos) || 0
    ]))
}


/**
 * Finds songs in by tag using strict matching
 */
exports.find = (needle, tag) => {
  return Instance
    .sendCommand( Instance.cmd('playlistfind', [
      Tags.validateWithFallback(tag),
      util.assureString(needle)
    ]), [
      Parsers.parseArrayMessage,
      Parsers.songArrayParser
    ])
    .map( util.normalizeObjectKeys )
    .then( res => {
      if(!_.isArray(res) || _.isEmpty(res))
        return []
      return res
    })
    .filter( song => song.id >= 0 )
}

/**
 * Searches songs for partial matches
 * is case insensitive
 */
exports.search = (needle, tag) => {
  return Instance
    .sendCommand( Instance.cmd('playlistsearch', [
      Tags.validateWithFallback(tag),
      util.assureString(needle)
    ]), [
      Parsers.parseArrayMessage,
      Parsers.songArrayParser
    ])
    .map( util.normalizeObjectKeys )
    .filter( song => song.id >= 0 )
}

/**
 * Sets the priority for song ids
 * song ids can can be overloaded
 * by args (single vals/arrays)
 */
exports.prioid = (priority, ...songids) => {
  let args = UTIL.toIntArray(songids)
  args.unshift( Parsers.parsePriority(priority) )
  return Instance.sendCommand( Instance.cmd('prioid', args) )
}


/**
 *
 */
exports.shuffle = (from, to) => {
  let args = util.fromToArgs(from, to, true)
  return Instance
    .sendCommand( Instance.cmd('shuffle', args) )
}

/**
 * Swap songs
 */
exports.swapid = (songid1, songid2) => {
  return Instance
    .sendCommand( Instance.cmd('swapid', [
      Parsers.parseInt(songid1) || 0,
      Parsers.parseInt(songid2) || 0
    ]))
}

/**
 * Adds tag to the song
 * only for remote songs
 */
exports.addtagid = (songid, tag, value) => {
  return P
    .resolve(Tags.isValid(tag))
    .then( valid => {
      if(!valid) return P.reject('ERR_INVALID_TAG')
      return Instance
        .sendCommand(Instance.cmd('addtagid', [
          Parsers.parseInt(songid),
          tag,
          util.assureString(value)
        ]))
    })
}

/**
 * Clears tag to the song
 * only for remote songs
 */
exports.cleartagid = (songid, tag) => {
  return P
    .resolve(Tags.isValid(tag))
    .then( valid => {
      if(!valid) return P.reject('ERR_INVALID_TAG')
      return Instance
        .sendCommand(Instance.cmd('cleartagid', [
          Parsers.parseInt(songid),
          tag
        ]))
    })
}

/**
 * Removes duplicates from the list
 */
exports.removeDuplicates = () => {
  return exports
    .list()
    .then( list => {
      let dupes = util.findDuplicates(list)

      if(_.isEmpty(dupes))
        return []

      Instance.startTransaction()
      for(let dupe of dupes)
        exports.deleteid(dupe.id)

      return Instance
        .commitTransaction()
        .then( () => dupes )
    })
}

