'use strict'

const Log = Logger.get(__filename)

const mpd = require('mpd')
const util = require('../util')
const setRxInstance = require('../rx/instance')

let mpdinstance = null

let TRANSACTION = null

class Iface {

  static get instance() {
    return mpdinstance
  }

  static get isConnected() {
    return !!this.instance
  }

  static connect(conf) {
    return P
      .resolve()
      .then( () => {

        if(this.isConnected)
          return true

        return connectAndHandle(conf)

      })
  }

  static disconnect() {
    return P
      .resolve()
      .then( () => {

        if(!this.isConnected)
          return false

        util.close(mpdinstance)
        mpdinstance = null
        return true
      })
  }

  /**
   * Wrapper for mpd's cmd method
   * it passes empty args if not set
   * to avoid the fails.
   *
   * @returns mpd.Command
   */
  static cmd(cmd_str, args) {
    if(_.isEmpty(args))
      args = []
    return mpd.cmd(cmd_str, args)
  }

  /**
   * Sends command to MPD
   * @param cmd mpd.Command
   * @param parsers optional array (or single fn)
   *        of parsers, which will be executed in
   *        order, each will get data from the
   *        previous one
   */
  static sendCommand(cmd, parsers) {

    if(TRANSACTION) {
      TRANSACTION.push(cmd)
      return P.resolve([])
    }

    return new P( (res, rej) => {
      if(!this.instance)
        return rej('ERR_NOT_CONNECTED_TO_MPD')

      this
        .instance
        .sendCommand(cmd, (err, data) => {
          if(err) return rej(err)
          res(data)
        })
    })
    .then( runParsersOnResult(parsers) )
  }

  /**
   * Sends multiple commands to MPD
   * @param cmd mpd.Command
   * @param parsers optional array (or single fn)
   *        of parsers, which will be executed in
   *        order, each will get data from the
   *        previous one
   */
  static sendCommands(cmds, parsers) {
    return new P( (res, rej) => {
      if(!this.instance)
        return rej('ERR_NOT_CONNECTED_TO_MPD')

      this
        .instance
        .sendCommands(cmds, (err, data) => {
          if(err) return rej(err)
          res(data)
        })
    })
    .then( runParsersOnResult(parsers) )
  }

  /**
   * If called, transaction will be started
   * meaning that all calls to sendCommant()
   * will be queued and executed after
   * commitTransaction() is called
   */
  static startTransaction() {
    if(!_.isArray(TRANSACTION))
      TRANSACTION = []
    return true
  }

  static commitTransaction(parsers) {
    return new P( (resolve, reject) => {
      if(_.isEmpty(TRANSACTION))
        return reject('ERR_NO_TRANSACTION_ACTIVE')
      resolve(this.sendCommands(TRANSACTION, parsers))
      TRANSACTION = null
    })
  }
}



const connectAndHandle = (conf) => {
  return new P( (res, rej) => {

    mpdinstance = new mpd.connect(conf)

    let onErr = err => {
      Log.e('connectAndHandle()#onErr', err)
      rej(err)
    }

    mpdinstance.on('error', onErr)

    mpdinstance.on('ready', () => {
      Log.i('connectAndHandle() mpd connected.')
      mpdinstance.removeListener('error', onErr)
      res(true)
    })

  })
}

/**
 * Helper to run parsers
 * on the data passed to the method
 */
const runParsersOnResult = (parsers) => {
  return data => {
    if(_.isFunction(parsers))
      parsers = [parsers]

    if(_.isEmpty(parsers))
      return data

    if(!_.isArray(parsers))
      parsers = [parsers]

    for(let parser of parsers)
      data = parser(data)

    return data
  }
}

module.exports = Iface
