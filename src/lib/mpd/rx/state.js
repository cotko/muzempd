'use strict'

const Log = Logger.get(__filename)

// State change subject

const Rx = require('rx')
const { Updater } = require('./')
const { MPD } = require('../')


/**
 * BehaviorSubject so that it holds the
 * initial value to which observers might
 * subscribe
 */
const subject = new Rx.BehaviorSubject({
  stats: {},
  current_song: null,
  state: 'unavailable'
})


/**
 * When should we refetch the state
 * of the mpd?
 */
const change$ = Updater
  .$
  .filter( evt => {
    let identifier = _.get(evt, 'evt', evt)
    return !!~[
      'connected',
      'mixer',
      'player',
      'options',
      'update'
    ].indexOf(identifier)
  })
  .debounce(5)


/**
 * Subscribe to state changes,
 * collect the data from various MPD apis
 * and notify the state subject with new data
 */
const subscription = change$.subscribe(
  () => {
    Rx
      .Observable
      .concat(
        Rx.Observable.fromPromise(MPD.api.Stats.get).map( stats => ({stats}) ),
        Rx.Observable.fromPromise(MPD.api.Status.get),
        Rx.Observable.fromPromise(MPD.api.Player.replayGainStatus),
        Rx.Observable.fromPromise(
          MPD.api.Status.currentsong).map( current_song => ({current_song})
        )
      )
      // reduce all data into one object using
      // lodash's merge operator
      .reduce( (acc, x, id, source) => _.merge(acc, x), {} )
      .subscribe(
        state => {
          // notify the state subject with newly
          // collected data
          subject.onNext(state)
        }
      )
  }
)



const shutdown = () => {
  Log.i('shutdown()')
  subscription.dispose()
  subject.onCompleted()
  subject.dispose()
}


module.exports = {
  shutdown,
  subject,
  change$
}
