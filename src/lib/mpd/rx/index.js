'use strict'

module.exports = {
  get Updater() { return require('./updater') },
  get Instance() { return require('./instance') },
  get State() { return require('./state') },
  get Stream() { return require('./stream') },
  get Playlist() { return require('./playlist') },
  get Playlists() { return require('./playlists') },
  get Discover() { return require('./discover') },
  get Progress() { return require('./progress') }
}
