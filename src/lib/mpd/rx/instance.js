'use strict'

const Log = Logger.get(__filename)

// rx js Observable for mpd instance
// this is same as Rx's .fromEvent()
// for node, only that it handles mpd events.

const Rx = require('rx')

let instanceObservable = null



// connect event is ignored, ready is important for us

// system events:
// database - the song database has been modified after update.
// update - a database update has started or finished. If the database was modified during the update, the database event is also emitted.
// stored_playlist - a stored playlist has been modified, renamed, created or deleted
// playlist - the current playlist has been modified
// player - the player has been started, stopped or seeked
// mixer - the volume has been changed
// output - an audio output has been enabled or disabled
// options - options like repeat, random, crossfade, replay gain
// sticker - the sticker database has been modified.
// subscription - a client has subscribed or unsubscribed to a channel
// message - a message was received on a channel this client is subscribed to; this event is only emitted when the queue is empty




const systemEvents = [
  'database',
  'update',
  'stored_playlist',
  'playlist',
  'player',
  'mixer',
  'output',
  'options',
  'sticker',
  'subscription',
  'message'
]

const getSystemEvtHandler = (evt, observer) => {
  return data => {
    console.log('got event', evt, data)
    if(!instanceObservable) return
    observer.onNext({
      evt,
      data,
      type: 'system'
    })
  }
}

const handleInstance = instance => {
  instanceObservable =  Rx.Observable.create( observer => {

    let errorHandler = err => {
      observer.onError(err)
    }

    let endHandler = () => {
      instanceObservable = null
      observer.onCompleted()
    }

    instance.on('end', endHandler)
    instance.on('error', errorHandler)

    let bound = []
    for(let sevt of systemEvents) {
      let listener = getSystemEvtHandler(sevt, observer)
      instance.on(`system-${sevt}`, listener)
      bound.push({
        listener,
        sevt
      })
    }

    return () => {
      instance.removeListener('end', endHandler)
      instance.removeListener('error', errorHandler)
      for(let listener of bound)
        instance.removeListener(
          `system-${listener.sevt}`,
          listener.listener
        )
    }
  }).publish().refCount()

}

const shutdown = () => {
  Log.i('shutdown()')
}

module.exports = {
  handleInstance,
  shutdown,
  get isAvailable() { return !!instanceObservable },
  get $() { return instanceObservable }
}
