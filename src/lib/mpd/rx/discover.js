'use strict'

const Log = Logger.get(__filename)

const Rx = require('rx')

// Discovery subject
// just a subject so that actual discovery
// lib can publish data over it

const subject = new Rx.BehaviorSubject({
  state: 'STOPPED',
  conf: {},
  autoconnect: {
    on: false,
    retry_count: 0
  }
})

const shutdown = () => {
  Log.i('shutdown()')
  subject.onCompleted()
}

const publishState = (state, conf) => {
  //subject.onNext({state, conf})
  let st = _.merge(
    subject.last().source.value, {
      state,
      conf
    })

  subject.onNext(st)
}

const publishAutoDiscovery = (on, retry_count) => {
  let st = _.merge(
    subject.last().source.value, {
      autoconnect: {
        on,
        retry_count
      }
    })

  subject.onNext(st)
}

module.exports = {
  publishState,
  publishAutoDiscovery,
  shutdown,
  subject
}
