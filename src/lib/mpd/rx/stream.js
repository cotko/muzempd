'use strict'

const Log = Logger.get(__filename)

// Stream of collected subjects
// combines state, playlist etc. subjects,
// and inits the Updater

const Rx = require('rx')
const {
  Updater,
  State,
  Playlist,
  Playlists,
  Discover,
  Progress
} = require('./')

const shutdown = () => {
  Log.i('shutdown()', 'shutting down mpd rx modules')
  return P
  .each([
    Updater,
    State,
    Playlist,
    Playlists,
    Discover,
    Progress
  ], s => {
    return s.shutdown()
  })
}

module.exports = {

  shutdown,

  get $() {

    return Rx
      .Observable
      .combineLatest(
        Updater.connection$,
        State.subject,
        Playlist.subject,
        Playlists.subject,
        Discover.subject,
        (connected, state, playlist, playlists, probe) => ({
          connected,
          state,
          playlist,
          playlists,
          probe,
          progress$: Progress.$
        })
      )
      // debounce for when MPD is
      // emitting various events
      // for a singular change
      .debounce(100)
  }
}
