'use strict'

const Log = Logger.get(__filename)

// Base observable for the MPD events
// - listens for new instance (when connected)
//    - triggers connected
//    - and disconnected when not available any more
// - fowrads the events from the ./instance
// on disconnect it starts to listen for new
// connection again

const Rx = require('rx')
const { Instance } = require('./')

/**
 * BehaviorSubject so that it holds the
 * initial value to which observers might
 * subscribe
 */
const subject = new Rx.BehaviorSubject({
  evt: 'disconnected'
})


let instanceListener = null
let forwardListener = null


/**
 * returns observable which completes
 * when the rx instance is available
 */
const discoverInstance = () =>
  Rx
    .Observable
    .interval( 500 )
    .filter( () => Instance.isAvailable )
    .take(1)
    .timeInterval()

const forwardInstanceEvents = () => {
  forwardListener = Instance
    .$
    .subscribe(
      evt => subject.onNext(evt),
      err => {
        console.log(err)
        subject.onNext({err})
      },
      () => {
        subject.onNext({ evt: 'disconnected' })

        // listen for instance again
        start()

      }
    )

}

const start = () => {
  instanceListener = discoverInstance()
    .subscribe(
      _.noop,
      _.noop,
      // on completed is when instance
      // is available
      () => {

        instanceListener.dispose()
        instanceListener = null

        subject.onNext({
          evt: 'connected'
        })

        // forward instance events to the
        // liseners
        forwardInstanceEvents()
      }

    )
}

/**
 * Connection available/not available
 */
const connection$ = subject
  // map connection events to boolean flags
  // and others to undefined
  .map( evt => {
    evt = _.get(evt, 'evt', evt)
    if('connected' === evt) return true
    if('disconnected' === evt) return false
  })
  // keep only true/false (connection related)
  .filter(_.negate(_.isNil))

const init = _.once(start)


const shutdown = () => {
  Log.i('shutdown()')
  if(instanceListener) instanceListener.dispose()
  if(forwardListener) forwardListener.dispose()
  subject.onCompleted()
  instanceListener = null
  forwardListener = null
}

module.exports = {
  shutdown,

  get connection$() {

    // init the updater if not
    // yet inited
    init()

    return connection$
  },

  get $() {

    // init the updater if not
    // yet inited
    init()

    return subject
  }
}
