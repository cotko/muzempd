'use strict'

// Progress subject
// binds to the state stream and resolves the current
// state. In case it thinks it's a song, it periodically
// updates the progress (fake, not real to avoid unneeded
// querying) and in case if it thinks it's a stream it
// sets the coorrect status.

// holdes: {
//   status: 'unavailable' / 'play' / 'streaming' / 'pause' / 'stop'
//   elapsed: num
//   total: num,
//   progress: num // percent
// }

const Log = Logger.get(__filename)

const Rx = require('rx')
const { State } = require('./')


const progress$ = Rx
  .Observable
  .combineLatest(

    // take state and time from a state
    // in case song is playing, save the
    // date as well to make it available
    // for the latter fake elapsed calculation
    State
      .subject
      .map( pstate => {
        let state = _.get(pstate, 'state', 'unavailable')
        let time = _.merge({}, {
          elapsed: 0,
          total: 0,
          progress: 0,
        }, _.get(pstate, 'time', {}))

        // streaming?
        let streaming = _
          .chain(pstate)
          .get('current_song.file')
          .startsWith('http')
          .value()

        if(streaming && 'play' === state) state = 'streaming'

        // set the timestamp only if actually playing
        // so it can be added to the current elapsed time
        let timestamp = null
        switch(state) {
          case 'streaming':
          case 'play':
            timestamp = Date.now()
            break
        }

        return _.merge({ timestamp, state }, time)

      })
      .debounce(100),

    // 1 sec interval updater just to
    // update the elapsed for when there
    // is no change on state stream
    new Rx.Observable
      .interval(1000)
      .startWith(0),

    (state, interval) => {

      let res = _.merge({}, state)
      if(res.timestamp) {
        let diff = Date.now() - res.timestamp
        res.elapsed = res.elapsed + Math.round(diff * .001)
      }

      if(res.total)
        res.progress = Math.round(
          res.elapsed / res.total * 10000
        ) / 100
      else
        res.progress = 0

      delete res.timestamp
      return res

    }
  )
  //.distinct( state => `${state.state}${state.progress}${state.elapsed}${state.total}` )


const shutdown = () => {
  Log.i('shutdown()')
}

module.exports = {
  shutdown,
  get $() { return progress$ }
}

