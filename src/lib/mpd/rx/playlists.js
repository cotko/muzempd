'use strict'

const Log = Logger.get(__filename)

// Playlist change subject
// fetches playlists on changes

const Rx = require('rx')
const { Updater } = require('./')
const { MPD } = require('../')

/**
 * BehaviorSubject so that it holds the
 * initial value to which observers might
 * subscribe
 */
const subject = new Rx.BehaviorSubject({
  list: []
})


/**
 * When should we refetch the playlists
 * of the mpd?
 */
const change$ = Updater
  .$
  .filter( evt => {
    let identifier = _.get(evt, 'evt', evt)
    return !!~[
      'stored_playlist',
      'connected'
    ].indexOf(identifier)
  })
  .debounce(5)


/**
 * Fetch playlists data on playlists change
 */
const subscription = change$.subscribe(
  () => {
    Rx
      .Observable
      .concat(
        Rx.Observable.fromPromise(MPD.api.Playlists.list).map( list => ({list}) )
      )
      // reduce all data into one object using
      // lodash's merge operator
      .reduce( (acc, x, id, source) => _.merge(acc, x), {} )
      .subscribe(
        state => {
          // notify the state subject with newly
          // collected data
          subject.onNext(state)
        }
      )
  }
)



const shutdown = () => {
  Log.i('shutdown()')
  subscription.dispose()
  subject.onCompleted()
  subject.dispose()
}


module.exports = {
  shutdown,
  subject,
  change$
}
