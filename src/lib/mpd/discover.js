'use strict'

const Log = Logger.get(__filename)

// discovery for the mpd server
const mpd = require('mpd')
const util = require('./util')
const SimpleStore = require('../store').Simple.Store
const store = new SimpleStore('mpd_probe')
const DiscoverSubject = require('./rx').Discover

// various probes
const SOCK_GLOBAL = '/var/run/mpd/socket'
const SOCK_USER = _.get(process, 'env.HOME') ?
  `${process.env.HOME}/.mpd/socket` : false

const DEFAULT_VIA_TCP = {
  port: 6600,
  host: 'localhost'
}

const getStoredConfig = () => {
  return store.get('connection')
}

const setStoredConfig = opts => {
  return store.set('connection', opts)
}

const probeConnect = (opts) => {
  let client = null
  DiscoverSubject.publishState('PROBING', opts)
  return new P( (res, rej) => {
    if(!opts) return res(false)
    if(opts.path) opts.path = UTIL.sanitizePath(opts.path)
    client = new mpd.connect(opts)
    client.on('ready', () => {
      res(true)
    })
    client.on('error', err => {
      res(false)
    })
  })
  .finally( () => {
    util.close(client)
  })
}

const getPreferredConfigs = configs => {
  return P
  .filter(
    sanitizeConfigs(configs)
    .concat([
      getStoredConfig(),
      SOCK_USER,
      SOCK_GLOBAL,
      DEFAULT_VIA_TCP
    ]), conf => {
      if(!conf) return false
      if(_.isString(conf)) {
        return UTIL
          .isSocket(UTIL.sanitizePath(conf))
          .catch( err => false )
      }
      return true
    })
}

const sanitizeConfigs = configs => {
  return _
    .chain(configs)
    .castArray()
    .flatten()
    .compact()
    .value()
}

/**
 * Additional configs can be passed
 * in array or just as one argument;
 * [ 'path/to/socket' / { port, host } ]
 */
const discover = configs => {
  let found = false

  Log.d('discover(), additional configs', configs)

  DiscoverSubject.publishState('STARTED')
  return getPreferredConfigs(configs)
    .each( conf => {

      if(found) return false

      if(_.isString(conf))
        conf = { path: UTIL.sanitizePath(conf) }

      Log.i('discover() trying with', conf)

      return probeConnect(conf)
      .then( ok => {
        if(ok) found = conf
        return ok
      })
    })
    .then( () => {
      if(found) {
        setStoredConfig(found)
        DiscoverSubject.publishState('FOUND', found)
        return found
      }
      DiscoverSubject.publishState('FAILED')
      return P.reject('MPD_CONNECTION_NOT_FOUND')
    })

}

module.exports = {
  discover,
  getPreferredConfigs,
  probeConnect,
  getStoredConfig
}
