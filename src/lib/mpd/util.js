'use strict'

const DEFAULT_DUPS_MATCH_KEYS = [
  'file',
  'name',
  'time',
  'title',
  'artist',
  'last_modified',
  'genre',
  'track'
]

const close = socket => {
  if(!socket) return
  if(socket.end) socket.end()
  if(socket.destroy) socket.destroy()

  // handle passing of mpd's
  // socket wrapper
  let sock = _.get(socket, 'socket')
  if(sock) close(sock)
}

/**
 * All keys are lower case
 * all non alpha chars are replaced
 * with `_`
 */
const normalizeObjectKeys = obj => {
  return UTIL.normalizeObjectKeys(obj)
}

/**
 * Find duplicates using keys from
 * by_keys array or uses default
 * matcher
 */
const findDuplicates = (list, by_keys) => {
  if(!_.isArray(list)) return []
  if(!_.isArray(by_keys)) by_keys = DEFAULT_DUPS_MATCH_KEYS
  let len = list.length
  let dupes = []
  let dupe_items = []
  for(let pos = 0; pos < len; pos++) {
    let item = list[pos]
    let dupeid = _.join(
      _.toArray(
        _.pick(item, by_keys)
      )
    )
    if(~dupes.indexOf(dupeid))
      dupe_items.push(item)
    else
      dupes.push(dupeid)
  }
  return dupe_items
}

/**
 * Makes sure needle string is set
 */
const assureString = needle => {
  // undefined and null check
  if(_.isNil(needle))
    return ''
  if(_.isString(needle))
    return needle
  return `${needle}`
}

/**
 * Helper to prepare args
 * [from]
 * or [from:to]
 * or [] if none are passed
 */
const fromToArgs = (from, to, both_needed=false) => {
  let args = []
  if(undefined !== from) from = _.parseInt(from)
  if(undefined !== to) to = _.parseInt(to)
  if(isNaN(from)) from = false
  if(isNaN(to)) to = false
  if(both_needed && (false === from || false === to))
    return args
  if(from !== false && from >= 0) {
    if(to > from) args.push(`${from}:${to}`)
    else args.push(from)
  }
  return args
}


exports = module.exports = {
  close,
  normalizeObjectKeys,
  findDuplicates,
  assureString,
  fromToArgs
}

