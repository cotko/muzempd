'use strict'

const Log = require('../').Logger.get(__filename)
// Error handler
// Can register the list of error codes
// to return additional info about the error

const ERR_LIST = {}
const ErrStream = require('./subject')

const code2msg = code => {
  code = `${code}`
  return _
    .chain(code)
    .replace(/[_-]+/g, ' ')
    .split(' ')
    .map( s =>
      s.substr(0, 1).toUpperCase() +
      s.substr(1, s.length).toLowerCase()
    )
    .join(' ')
    .value()
}

export class Err {

  static registerErrors(errs) {
    for(let err in errs) {
      let info = errs[err] || {}
      if(!info.msg) msg = code2msg(err)
      if(!info.info) info.info = {}
      info.code = err
      ERR_LIST[err] = info
    }
  }

  static fromCode(code, info) {
    code = `${code}`

    let inf = ERR_LIST[code] || {
      code,
      info: {},
      msg: code2msg(code)
    }

    let err = new Error(inf.msg)
    err.code = inf.code
    err.info = info || inf.info

    return err
  }

  static registerErr(err) {
    ErrStream.publishError(err)
  }

  static get $() {
    return ErrStream
  }

  static dispose() {
    Log.i('dispose()')
    return this.$.shutdown()
  }

}
