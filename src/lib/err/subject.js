'use strict'

const Rx = require('rx')

const ERRS = {
  last: null,
  errors: []
}


// Errors subject
// just a subject for the errors to be
// published to

const subject = new Rx.BehaviorSubject(ERRS)

const shutdown = () => {
  subject.onCompleted()
}

const publishError = err => {
  ERRS.last = err
  ERRS.errors.push(err)
  subject.onNext(ERRS)
}

const clear = () => {
  ERRS.last = null
  ERRS.errors.length = 0
  subject.onNext(ERRS)
}

module.exports = {
  publishError,
  clear,
  shutdown,
  subject
}
